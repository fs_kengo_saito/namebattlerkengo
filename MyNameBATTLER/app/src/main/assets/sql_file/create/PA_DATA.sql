﻿CREATE TABLE PA_TBL
(
_pa_id INTEGER PRIMARY KEY,
pa_name TEXT NOT NULL,
pa_con TEXT NOT NULL,
pa_eff TEXT NOT NULL
)
/
INSERT INTO PA_TBL
(_pa_id,pa_name,pa_con,pa_eff)
values
(201,"リジェネレート","【発動条件】自パーティーのSP：40以上","ターン開始時、自身の最大HPの3％分を回復")
/
INSERT INTO PA_TBL
(_pa_id,pa_name,pa_con,pa_eff)
values
(202,"毒の刃","【発動条件】自パーティーのSP：50以上","相手への攻撃時に状態異常：毒を付与")
/
INSERT INTO PA_TBL
(_pa_id,pa_name,pa_con,pa_eff)
values
(203,"戦術指揮","【発動条件】自パーティーのSP：50以上","味方全体のAGI＆DEXを15％アップ")
/
INSERT INTO PA_TBL
(_pa_id,pa_name,pa_con,pa_eff)
values
(204,"底力","【発動条件】自身のHP：20％未満","自身のSTR＆DEF＆AGL＆DEXを20％アップする")
/
INSERT INTO PA_TBL
(_pa_id,pa_name,pa_con,pa_eff)
values
(205,"スロースターター","【発動条件】戦闘開始時～4ターン目まで","戦闘開始時にSTR＆DEF＆AGL＆DEX10％マイナス。4ターン目にSTR＆DEF＆AGL＆DEX20％アップ")
/
INSERT INTO PA_TBL
 (_pa_id,pa_name,pa_con,pa_eff)
 values
 (206,"プレッシャー","【発動条件】自パーティーのSP：80以上","相手全体のSTR＆DEF＆AGL＆DEXを15％ダウンする")
 /
 INSERT INTO PA_TBL
 (_pa_id,pa_name,pa_con,pa_eff)
 values
 (207,"一番槍","【発動条件】戦闘開始時","戦闘開始時の初期SP+20＆1ターン目のみ自身のAGL50％アップ")