CREATE TABLE SKILL_TBL
(
_skill_id TEXT PRIMARY KEY,
skill_name TEXT NOT NULL,
skill_cost INTEGER NOT NULL,
skill_eff TEXT NOT NULL
)
/
INSERT INTO SKILL_TBL
(_skill_id,skill_name,skill_cost,skill_eff)
values
("FIG001","渾身の一撃",15,"相手単体に125％の攻撃力で攻撃")
/
INSERT INTO SKILL_TBL
(_skill_id,skill_name,skill_cost,skill_eff)
values
("FIG002","シールドバッシュ",40,"相手単体に70％の攻撃力で攻撃し、50％の確率で相手を1ターン気絶させる")
/
INSERT INTO SKILL_TBL
(_skill_id,skill_name,skill_cost,skill_eff)
values
("WIZ001","ファイア",15,"相手単体に120％の攻撃力で防御力無視の攻撃")
/
INSERT INTO SKILL_TBL
(_skill_id,skill_name,skill_cost,skill_eff)
values
("WIZ002","マジックチャージ",45,"次回攻撃スキルを使用時の威力を25％アップ＆効果を全体化")
/
INSERT INTO SKILL_TBL
(_skill_id,skill_name,skill_cost,skill_eff)
values
("CLR001","ヒーリング",20,"最も残りHPが少ない味方単体のHPを20％回復")
/
INSERT INTO SKILL_TBL
(_skill_id,skill_name,skill_cost,skill_eff)
values
("CLR002","オールキュア",30,"自パーティー全体の状態異常を回復")
/
INSERT INTO SKILL_TBL
(_skill_id,skill_name,skill_cost,skill_eff)
values
("KNI001","鼓舞",0,"自パーティーのSPを25増加する")
/
INSERT INTO SKILL_TBL
(_skill_id,skill_name,skill_cost,skill_eff)
values
("KNI002","かばう",15,"次回行動までの間、他のキャラへの単体攻撃を自身に集中させる")
/
INSERT INTO SKILL_TBL
(_skill_id,skill_name,skill_cost,skill_eff)
values
("HUN001","スプレッドショット",25,"相手全体に60％のSTRで攻撃")
/
INSERT INTO SKILL_TBL
(_skill_id,skill_name,skill_cost,skill_eff)
values
("HUN002","スモークショット",40,"相手単体に80％の攻撃力で攻撃し、状態異常：暗闇を与える")
/
INSERT INTO SKILL_TBL
(_skill_id,skill_name,skill_cost,skill_eff)
values
("WAR001","捨て身の特攻",50,"相手単体に250％のSTRで攻撃。使用後～次の行動までDEF・AGIが0になる")
/
INSERT INTO SKILL_TBL
(_skill_id,skill_name,skill_cost,skill_eff)
values
("WAR002","ウォークライ",0,"自身のHPを最大HP30％分消費し、自パーティーのSPを50増加する")
/
INSERT INTO SKILL_TBL
(_skill_id,skill_name,skill_cost,skill_eff)
values
("CHE001","覚醒",125,"HP以外の全ステータスを戦闘終了まで100％アップ（1回の戦闘で1度のみ使用可能）")
/
INSERT INTO SKILL_TBL
(_skill_id,skill_name,skill_cost,skill_eff)
values
("CHE002","スキル封じ",50,"相手単体に50％の攻撃力で攻撃し、80％の確率で相手に状態異常：封印を与える")