CREATE TABLE ORDER_TBL
(
_ORDER_ID INTEGER PRIMARY KEY,
ORDER_NAME TEXT NOT NULL,
ATTACK_TARGET TEXT NOT NULL,
ORDER_DESCRIPTION TEXT NOT NULL,
ATTACK_PROBABILITY INTEGER NOT NULL,
GUARD_PROBABILITY INTEGER NOT NULL,
SKILL_USE_CHECK INTEGER NOT NULL
)
/
INSERT INTO ORDER_TBL
(_ORDER_ID,ORDER_NAME,ATTACK_TARGET,ORDER_DESCRIPTION,ATTACK_PROBABILITY,GUARD_PROBABILITY,SKILL_USE_CHECK)
values
(301,
"ガンガン行こうぜ",
"相手パーティーの1番目にいるキャラクターを攻撃",
"攻撃率：100％、防御率：0％、スキル：使用できる状態なら即使用する",
100,
0,
1
)
/
INSERT INTO ORDER_TBL
(_ORDER_ID,ORDER_NAME,ATTACK_TARGET,ORDER_DESCRIPTION,ATTACK_PROBABILITY,GUARD_PROBABILITY,SKILL_USE_CHECK)
values
(302,
"堅実に戦え",
"相手パーティーの2番目にいるキャラクターを攻撃",
"攻撃率：50％、防御率：50％、スキル：SPが70未満では使用せず、SPが70以上で使用できる状態なら使用する",
50,
50,
1
)
/
INSERT INTO ORDER_TBL
(_ORDER_ID,ORDER_NAME,ATTACK_TARGET,ORDER_DESCRIPTION,ATTACK_PROBABILITY,GUARD_PROBABILITY,SKILL_USE_CHECK)
values
(303,
"守りに徹しろ",
"相手パーティーの3番目にいるキャラクターを攻撃",
"攻撃率：0％、防御率：100％、スキル：SPが100未満では使用せず、SPが100以上で使用できる状態なら使用する",
0,
100,
1
)
/
INSERT INTO ORDER_TBL
(_ORDER_ID,ORDER_NAME,ATTACK_TARGET,ORDER_DESCRIPTION,ATTACK_PROBABILITY,GUARD_PROBABILITY,SKILL_USE_CHECK)
values
(304,
"SPを溜めろ",
"相手パーティーで最も残HPが低いキャラクターを攻撃",
"攻撃率：30％、防御率：70％、スキル：使用しない",
30,
70,
0
)
/
INSERT INTO ORDER_TBL
(_ORDER_ID,ORDER_NAME,ATTACK_TARGET,ORDER_DESCRIPTION,ATTACK_PROBABILITY,GUARD_PROBABILITY,SKILL_USE_CHECK)
values
(305,
"バランスよく戦え",
"相手パーティーで最も残HPが多いキャラクターを攻撃",
"攻撃率：70％、防御率：30％、スキル：SPが50未満では使用せず、SPが50以上で使用できる状態なら使用する",
70,
30,
1
)