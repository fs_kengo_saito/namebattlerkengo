package com.example.mynamebattler.PlayerPackage;

import com.example.mynamebattler.ManagerPackage.PAManager;
import com.example.mynamebattler.ManagerPackage.SkillManager;
import com.example.mynamebattler.PartyPackage.Party;

public interface PlayerAction {

    SkillManager skillManager = SkillManager.getInstance();
    PAManager paManager = PAManager.getInstance();

    //攻撃用メソッド
    default void Attack(PlayerData actionMember, PlayerData target){
        actionMember.setActLog(actionMember.getPlayerName() + "の攻撃！\n");
        int damage = Calculation.DamageCalculation(actionMember,target);
        target.HitDamage(damage);
        actionMember.setActLog(target.getPlayerName() + "に" + damage +"のダメージ\n\n");
        if(actionMember.attackPoison){
            target.statePoison = true;
            actionMember.setActLog(target.getPlayerName() + "は毒になった。\n");
        }
        target.deadCheck();
    }

    //防御用メソッド
    default void Guard(PlayerData actionMember){
        //防御状態にする
        actionMember.guardCheck = true;
        actionMember.setActLog(actionMember.getPlayerName() + "は防御した！\n\n");
    }

    //スキル用メソッド
    default void Skill(String skillId,PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){

        //封印状態かチェック
        if(actionMember.isStateSealed()){
            actionMember.setActLog(actionMember.getPlayerName() + "はスキルは封じられている！\n\n");
        } else {
            skillManager.InvokeSkill(skillId,target,actionMember,myParty,enemyParty);
            target.deadCheck();
        }
    }

    //JA用メソッド(職業ごとで定義する為、抽象メソッドとする)
    void JA(PlayerData actionMember, Party myParty, Party enemyParty);

    //PA用メソッド
    default void PA(PlayerData actionMember, Party myParty, Party enemyParty){
        paManager.InvokePA(actionMember.paId,actionMember,myParty,enemyParty);
    }
}
