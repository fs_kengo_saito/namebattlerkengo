package com.example.mynamebattler.PlayerPackage.Job;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
職業：ウィザード(Playerクラスを継承)
 */
public class Wizard extends PlayerData implements Constant, Serializable {

    public Wizard(String name){
        super(name);
    }

    //名前から取得したステータスでCharacter作成
    @Override
    protected void CharaCreate(){
        int randomIndex = (int)(Math.random()*10);
        defaultHp = (GetNumber(randomIndex,STATUS_RANGE_BASE) + STATUS_RANGE_D) * STATUS_HP_FACTOR;
        defaultStr = GetNumber(randomIndex+1,STATUS_RANGE_BASE) + STATUS_RANGE_C;
        defaultDef = GetNumber(randomIndex+2,STATUS_RANGE_BASE) + STATUS_RANGE_D;
        defaultAgi = GetNumber(randomIndex+3,STATUS_RANGE_BASE) + STATUS_RANGE_B;
        defaultDex = GetNumber(randomIndex+4,STATUS_RANGE_BASE) + STATUS_RANGE_C;
        defaultLuck = GetNumber(randomIndex+5,STATUS_RANGE_BASE) + STATUS_RANGE_B;
    }

    //JA用メソッド
    @Override
    public void JA(PlayerData actionMember, Party myParty, Party enemyParty){
        if(myParty.getSp() >= 40) {
            actLog.append(getPlayerName() + "のJA発動！\n受ける全てのダメージをカット！\n\n");
            setJACheck(true);
        } else {
            setJACheck(false);
        }
    }

    //被ダメージ
    @Override
    public void HitDamage(int damage){
        double lastDamage = damage;
        //JA発動中ならダメージ軽減
        if(isJACheck()){
            lastDamage = lastDamage * 0.75;
        }
        //防御中ならダメージ軽減
        if(guardCheck){
            lastDamage = lastDamage * 0.7;
        }
        battleHp = Math.max(battleHp - (int)lastDamage,0);
    }

}
