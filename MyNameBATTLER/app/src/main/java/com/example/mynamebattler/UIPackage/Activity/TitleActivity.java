package com.example.mynamebattler.UIPackage.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.DBPackage.DBManager;
import com.example.mynamebattler.ManagerPackage.ImageManager;
import com.example.mynamebattler.ManagerPackage.SoundManager;
import com.example.mynamebattler.R;

/*
　タイトル画面の表示及びUI処理を行うクラス
 */
public class TitleActivity extends AppCompatActivity implements Constant {

    private DBManager dbManager;
    private ImageManager imageManager;
    private SoundManager soundManager;

    //端末の戻るを禁止する
    @Override
    public void onBackPressed(){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.title_activity);

        //各Managerのインスタンスを取得
        dbManager = DBManager.getInstance();
        imageManager = ImageManager.getInstance();
        soundManager = SoundManager.getInstance();

        //タイトルBGMの再生
        soundManager.TitleBGMStart(this);
        setVolumeControlStream(AudioManager.STREAM_MUSIC); //音量調整を端末側で出来る様にしておく

        InitTitleLogo();
        InitButtonUI();

    }

    //Activityが隠れた時に呼び出し
    @Override
    protected void onPause(){
        super.onPause();
        soundManager.BGMPause();
    }

    //Activityが前面に表示された時に呼び出し
    protected void onResume(){
        super.onResume();

        //DBの作成
        dbManager.CreateDB(this);

        //BGMの再開
        soundManager.TitleBGMStart(this);
    }

    //Activityが破棄されるタイミングBGMを完全停止する
    @Override
    protected void onDestroy(){
        super.onDestroy();
        soundManager.BGMStop();
    }

    //タイトルロゴの表示
    private void InitTitleLogo(){
        ImageView titleLego = findViewById(R.id.TitleLogView);
        Bitmap image = imageManager.GetImageBitmap(this,TITLE_IMAGE_FILENAME);
        titleLego.setImageBitmap(image);
    }

    //ボタンの設定＆処理
    private void InitButtonUI(){

        //キャラクター一覧画面へ遷移
        ImageButton characterCreateBtn = findViewById(R.id.SendCharListActButton);
        Bitmap btnImage1 = imageManager.GetImageBitmap(this,CHARACTER_CREATE_BUTTON_IMAGE);
        characterCreateBtn.setImageBitmap(btnImage1);
        characterCreateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(),CharacterListActivity.class);
                startActivity(intent);
            }
        });

        //パーティー編成画面へ遷移
        ImageButton battleBtn = findViewById(R.id.SendPartyForActButton);
        Bitmap btnImage2 = imageManager.GetImageBitmap(this,BATTLE_BUTTON_IMAGE);
        battleBtn.setImageBitmap(btnImage2);
        battleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(),PartyFormationActivity.class);
                startActivity(intent);
            }
        });
    }
}
