package com.example.mynamebattler.UIPackage.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.ManagerPackage.BattleManager;
import com.example.mynamebattler.ManagerPackage.ImageManager;
import com.example.mynamebattler.ManagerPackage.PartyManager;
import com.example.mynamebattler.ManagerPackage.SkillManager;
import com.example.mynamebattler.ManagerPackage.SoundManager;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;
import com.example.mynamebattler.R;
import com.example.mynamebattler.SkillPackage.Skill;
import com.example.mynamebattler.UIPackage.Fragment.BattleCommandDialogFragment;
import com.example.mynamebattler.UIPackage.Fragment.EnemyPartyFragment;
import com.example.mynamebattler.UIPackage.Fragment.MyPartyFragment;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
/*
　バトルメイン画面の表示及びUI処理を行うクラス
 */
public class BattleSceneActivity extends AppCompatActivity implements Constant {

    private BattleManager battleManager;
    private SoundManager soundManager;
    private Timer timer;
    private BattleTimerTask battleTimerTask;
    private Party myParty,enemyParty;
    private Button battleCommandBtn,autoBattleBtn,battleMenuBtn;
    private ScrollView scrollBattleLogView;
    private TextView battleLog,myPartySPText,enemyPartySPText,nextCharacterText,nowOrderText;
    private Bundle bundle;
    private MyPartyFragment myPartyFragment;
    private EnemyPartyFragment enemyPartyFragment;

    //端末の戻るを禁止する
    @Override
    public void onBackPressed(){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.battle_scene_activity);

        //Bundleの初期化
        bundle = new Bundle();

        //各種インスタンスの取得
        battleManager = BattleManager.getInstance();
        soundManager = SoundManager.getInstance();

        //バトルBGMの再生
        soundManager.BattleBGMStart(this);

        //バトルを開始
        battleManager.battleStart();

        myParty = battleManager.getMyParty(); //自分のパーティーを取得
        enemyParty = battleManager.getEnemyParty(); //敵のパーティーを取得

        InitMyPartyTextUI();
        InitEnemyPartyTextUI();
        InitButtonUI();
        InitBattleLog();
        InitSPText();
        BattleTimer();

    }

    //Activityが隠れた時に呼び出し
    @Override
    protected void onPause(){
        super.onPause();
        soundManager.BGMPause();
    }

    //BattleOrderActivityから戻ってきた際に呼び出す
    @Override
    protected void onResume(){
        super.onResume();
        //BGMの再開
        soundManager.BGMReStart();
        BattleTimer(); //バトルタイマー再開
    }

    //Activityが破棄されるタイミングでサービスを終了させる
    @Override
    protected void onDestroy(){
        super.onDestroy();
        soundManager.BGMStop();
    }

    //プレイヤー側パーティーテキストの設定
    private void InitMyPartyTextUI(){
        myPartyFragment = new MyPartyFragment();

        //Bundleへパーティー情報を格納
        bundle.putSerializable(MYPARTY_PLAYER1_KEY,myParty.getParty().get(0));
        bundle.putSerializable(MYPARTY_PLAYER2_KEY,myParty.getParty().get(1));
        bundle.putSerializable(MYPARTY_PLAYER3_KEY,myParty.getParty().get(2));

        //MyPartyFragmentにパーティーを送り、更新
        myPartyFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mypartyFlame,myPartyFragment);
        transaction.commit();
    }

    //エネミー側パーティーテキストの設定
    private void InitEnemyPartyTextUI(){

        enemyPartyFragment = new EnemyPartyFragment();

        //Bundleへパーティー情報を格納
        bundle.putSerializable(ENEMYPARTY_PLAYER1_KEY,enemyParty.getParty().get(0));
        bundle.putSerializable(ENEMYPARTY_PLAYER2_KEY,enemyParty.getParty().get(1));
        bundle.putSerializable(ENEMYPARTY_PLAYER3_KEY,enemyParty.getParty().get(2));

        //EnemyPartyFragmentにパーティーを送り、更新
        enemyPartyFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.enemypartyFlame,enemyPartyFragment);
        transaction.commit();

    }

    //ボタンの設定＆処理
    private void InitButtonUI(){

        //バトルコマンドを開く
        battleCommandBtn = findViewById(R.id.battleCommandButton);
        battleCommandBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //敵の名前一覧を取得し、バンドルに格納
                ArrayList<String> list = new ArrayList<String>();
                for(PlayerData playerData :enemyParty.getParty()){
                    list.add(playerData.getPlayerName());
                }
                bundle.putStringArrayList(ENEMY_LIST_KEY,list);

                //スキル情報の取得
                ArrayList<Skill> allSkill = new ArrayList<Skill>();
                allSkill = battleManager.getActionCharacter().getSkillList();

                ArrayList<String> allSkillId = new ArrayList<String>();
                ArrayList<String> allSkillName = new ArrayList<String>();
                ArrayList<Integer> allSkillCost = new ArrayList<Integer>();
                ArrayList<String> allSkillEff = new ArrayList<String>();

                for(Skill skill:allSkill){
                    allSkillId.add(skill.getSkillId());
                    allSkillName.add(skill.getSkillName());
                    allSkillCost.add(skill.getSkillCost());
                    allSkillEff.add(skill.getSkillEff());
                }

                //Bundleに選択している職業のスキル一覧を格納
                bundle.putStringArrayList(SKILL_ID_LIST_KEY,allSkillId);
                bundle.putStringArrayList(SKILL_NAME_LIST_KEY,allSkillName);
                bundle.putIntegerArrayList(SKILL_COST_LIST_KEY,allSkillCost);
                bundle.putStringArrayList(SKILL_EFF_LIST_KEY,allSkillEff);

                //バトルコマンドダイアログを表示
                DialogFragment battleCommandDialogFragment = new BattleCommandDialogFragment();
                battleCommandDialogFragment.setArguments(bundle);
                battleCommandDialogFragment.show(getSupportFragmentManager(),"battle_command_dialog");
            }
        });

        //バトルメニュー画面へ遷移
        battleMenuBtn = findViewById(R.id.SendBattleOrderActButton);
        battleMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                Intent intent = new Intent(getApplication(), BattleMenuActivity.class);
                startActivity(intent);
            }
        });

        //オートモード切替ボタン
        autoBattleBtn = findViewById(R.id.AutoBattleButton);
        autoBattleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //オートバトルフラグがON(True)ならOffに切り替え
                if(battleManager.isAutoBattleFrag()){
                    battleManager.AutoModeSwitch();
                    autoBattleBtn.setText(R.string.AutoBattleButtonOffText);
                } else {
                    //オートバトルフラグがOFF(False)なら確認ダイアログを表示し、はいを押下でOnに切り替え
                    new AlertDialog.Builder(BattleSceneActivity.this).
                            setTitle("確認").
                            setMessage("オートモードに切り替えますか？").
                            setPositiveButton("はい", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    battleManager.AutoModeSwitch();
                                    autoBattleBtn.setText(R.string.AutoBattleButtonOnText);
                                }
                            }).
                            setNegativeButton("いいえ",null).
                            show();
                }
            }
        });
    }

    //バトルログの表示
    private void InitBattleLog(){
        scrollBattleLogView = findViewById(R.id.battleLog);
        battleLog = findViewById(R.id.battleLgoText);

        //バトルログへテキストを反映
        battleLog.setText(battleManager.getBattleLog().toString());
    }

    //SPの表示
    private void InitSPText(){
        myPartySPText = findViewById(R.id.myPartySPText);
        enemyPartySPText = findViewById(R.id.enemyPartySPText);

        myPartySPText.setText("MY SP：" + myParty.getSp());
        enemyPartySPText.setText("ENEMY SP：" + enemyParty.getSp());
    }

    //次に行動するキャラクターを表示
    private void InitNextCharacterText(){
        nextCharacterText = findViewById(R.id.NextCharacterText);
        nextCharacterText.setText("NEXT ACTION⇒ " + battleManager.getNextActionCharacterName());
    }

    //現在の作成を表示
    private void InitNowOrderText(){
        nowOrderText = findViewById(R.id.NowOrderText);

        nowOrderText.setText("【作戦】" + myParty.getOrder().getOrderName());
    }

    //UIを無効化する
    private void DisableUI(){
        battleCommandBtn.setEnabled(false);
        battleMenuBtn.setEnabled(false);
    }

    //UIを有効化する
    private void EnableUI(){
        battleCommandBtn.setEnabled(true);
        battleMenuBtn.setEnabled(true);
    }

    //バトル開始時から定期的にBattleTimerTaskを実行する
    private void BattleTimer(){
        if(timer == null){
            timer = new Timer();
            battleTimerTask = new BattleTimerTask(getApplication());
            timer.schedule(battleTimerTask,100,100); //開始から1秒後～1秒毎にゲーム終了を判定
        } else {
            timer.cancel();
            timer = null;
            timer = new Timer();
            battleTimerTask = new BattleTimerTask(getApplication());
            timer.schedule(battleTimerTask,100,100); //開始から1秒後～1秒毎にゲーム終了を判定
        }

    }

    //定期的に実行する処理
    public class BattleTimerTask extends TimerTask{

        private Context context;
        private Handler handler = new Handler();

        public BattleTimerTask(Context context){
            this.context = context;
        }

        @Override
        public void run(){
            handler.post(new Runnable() {
                @Override
                public void run() {
                    //バトル中に変動する各種UIを更新
                    InitBattleLog();
                    InitSPText();
                    InitMyPartyTextUI();
                    InitEnemyPartyTextUI();
                    InitNextCharacterText();
                    InitNowOrderText();
                    scrollBattleLogView.fullScroll(scrollBattleLogView.FOCUS_DOWN); //バトルログを最下部へスクロール

                    //待機中かどうか判定
                    if(battleManager.isWaitFrag()){
                        //待機中ならUI無効化
                        DisableUI();
                    } else {
                        //待機中で無ければUI有効化
                        EnableUI();
                    }
                    //どちらかの勝利が決まった場合
                    if(battleManager.isMyPartyWin() || battleManager.isEnemyPartyWin()){
                        DisableUI(); //UIを無効化
                        timer.cancel(); //タイマーを止める
                        //5秒後に画面遷移
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(context, BattleResultActivity.class);
                                startActivity(intent);
                            }
                        },5000);
                    }
                }
            });
        }
    }
}
