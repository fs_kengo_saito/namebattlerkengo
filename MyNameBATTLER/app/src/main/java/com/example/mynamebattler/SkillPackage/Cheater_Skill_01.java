package com.example.mynamebattler.SkillPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　転生者 - スキル1
　覚醒
 */
public class Cheater_Skill_01 extends Skill implements Serializable {

    @Override
    public void Invoke(PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){
        actionMember.setActLog(actionMember.getPlayerName() + "は" + skillName + "を使用した！\n");
        if(myParty.getSp() < skillCost){
            //SPが足りない場合
            actionMember.setActLog("しかしSPが足らなかった…\n\n");
        } else if(actionMember.isSkillCheck()) {
            //既にスキルが使用済みだった場合
            actionMember.setActLog("これ以上スキルは使えない…\n\n");
        } else {
            //スキル使用可能な場合
            myParty.SPCountUpdate(-skillCost);
            actionMember.setActLog(actionMember.getPlayerName() + "に宿る、前世の記憶が覚醒する！\n\n");

            //HPを全回復
            actionMember.setBattleHp(actionMember.getDefaultHp());

            //全能力を2倍
            actionMember.setBattleStr(actionMember.getDefaultStr() * 2);
            actionMember.setBattleDef(actionMember.getDefaultDef() * 2);
            actionMember.setBattleAgi(actionMember.getDefaultAgi() * 2);
            actionMember.setBattleDex(actionMember.getDefaultDex() * 2);
            actionMember.setBattleLuck(actionMember.getDefaultLuck() * 2);

            //スキル使用済みチェック
            actionMember.setSkillCheck(true);
        }
    }
}
