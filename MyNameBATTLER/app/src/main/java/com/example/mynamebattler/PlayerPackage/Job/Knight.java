package com.example.mynamebattler.PlayerPackage.Job;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
職業：ナイト(Playerクラスを継承)
 */
public class Knight extends PlayerData implements Constant, Serializable {

    public Knight(String name){
        super(name);
    }

    //名前から取得したステータスでCharacter作成
    @Override
    protected void CharaCreate(){
        int randomIndex = (int)(Math.random()*10);
        defaultHp = (GetNumber(randomIndex,STATUS_RANGE_BASE) + STATUS_RANGE_B) * STATUS_HP_FACTOR;
        defaultStr = GetNumber(randomIndex+1,STATUS_RANGE_BASE) + STATUS_RANGE_C;
        defaultDef = GetNumber(randomIndex+2,STATUS_RANGE_BASE) + STATUS_RANGE_A;
        defaultAgi = GetNumber(randomIndex+3,STATUS_RANGE_BASE) + STATUS_RANGE_E;
        defaultDex = GetNumber(randomIndex+4,STATUS_RANGE_BASE) + STATUS_RANGE_B;
        defaultLuck = GetNumber(randomIndex+5,STATUS_RANGE_BASE) + STATUS_RANGE_D;
    }

    //JA用メソッド
    @Override
    public void JA(PlayerData actionMember, Party myParty, Party enemyParty){
        //現在HPがHP最大値の80％以上で発動
        if(battleHp >= defaultHp * 0.8) {
            actLog.append(getPlayerName() + "のJA発動！\n毎ターンSPが少し増加！\n\n");
            myParty.SPCountUpdate(5);
        }
    }

}
