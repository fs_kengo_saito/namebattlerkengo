package com.example.mynamebattler.UIPackage.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import com.example.mynamebattler.AdapterPackage.SkillListViewAdapter;
import com.example.mynamebattler.Constant;
import com.example.mynamebattler.ManagerPackage.BattleManager;
import com.example.mynamebattler.R;
import com.example.mynamebattler.UIPackage.Activity.CharacterInfoActivity;

import java.util.ArrayList;
/*
　バトルコマンド - スキル一覧ダイアログを管理するクラス
 */
public class SkillCommandListFragment  extends DialogFragment implements Constant {


    private View view;

    private Bundle bundle;
    private ArrayList<String> allSkillId, allSkillName, allSkillEff;
    private ArrayList<Integer> allSkillCost;
    private String[] skillIds,skillNames,skillEffs;
    private Integer[] skillCosts;
    private String selectedSkillId;

    private BattleManager battleManager;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){

        //インスタンスを取得
        battleManager = BattleManager.getInstance();

        //バンドルから各種データを取得
        bundle = getArguments();
        GetBundleItems();

        //ダイアログビルダーを作成
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //viewの取得
        view = LayoutInflater.from(getActivity()).inflate(R.layout.skill_list_fragment,null);

        builder.setView(view)
                .setCustomTitle(GetCustomTitle(view))
                .setItems(skillNames, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //選択したスキルのIDを格納
                        selectedSkillId = skillIds[which];

                        //スキル使用確認ダイアログを表示
                        new AlertDialog.Builder(view.getContext())
                                .setTitle("このスキルを使用する？")
                                .setMessage(skillNames[which] + "\n消費SP：" + skillCosts[which] + "\n\n" + skillEffs[which])
                                .setPositiveButton("はい", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //はいを押下でスキル使用
                                        //選択したスキルIDをbattleManagerへセット
                                        battleManager.setSkillId(selectedSkillId);
                                        //スキル使用フラグをON
                                        battleManager.setUsedSkillFrag(true);
                                        //行動(スキル使用)
                                        battleManager.ActionFixed(SKILL_ID);
                                    }
                                })
                                .setNegativeButton("いいえ", null)
                                .show();
                        }
                    })
                .setNegativeButton("閉じる",null);

        return builder.create();
    }

    //ダイアログのタイトルを作成
    private TextView GetCustomTitle(View view){
        TextView customTitle = new TextView(view.getContext());
        customTitle.setText(R.string.SkillListTile); //表示テキスト
        customTitle.setGravity(Gravity.CENTER_HORIZONTAL); //中央揃え
        customTitle.setTextSize(24);
        customTitle.setTypeface(Typeface.DEFAULT_BOLD); //太字

        return customTitle;
    }

    private void GetBundleItems(){
        //スキル表示用アイテムを取得
        allSkillId = bundle.getStringArrayList(SKILL_ID_LIST_KEY);
        allSkillName = bundle.getStringArrayList(SKILL_NAME_LIST_KEY);
        allSkillCost = bundle.getIntegerArrayList(SKILL_COST_LIST_KEY);
        allSkillEff = bundle.getStringArrayList(SKILL_EFF_LIST_KEY);

        skillIds = allSkillId.toArray(new String[allSkillId.size()]);
        skillNames = allSkillName.toArray(new String[allSkillName.size()]);
        skillCosts = allSkillCost.toArray(new Integer[allSkillCost.size()]);
        skillEffs = allSkillEff.toArray(new String[allSkillEff.size()]);

    }
}
