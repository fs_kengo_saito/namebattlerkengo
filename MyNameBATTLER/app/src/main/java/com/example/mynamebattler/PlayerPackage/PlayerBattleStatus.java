package com.example.mynamebattler.PlayerPackage;

import com.example.mynamebattler.Constant;

/*
　プレイヤー毎のバトル中のステータスを管理するクラス
 */
public abstract class PlayerBattleStatus extends PlayerMainStatus implements Constant {

    //基本ステータス
    protected  int battleHp; //体力
    protected  int battleStr; //攻撃力
    protected  int battleDef; //ディフェンス
    protected  int battleDex; //器用さ
    protected  int battleAgi; //速さ
    protected  int battleLuck; //運
    protected boolean enemyFrag; //敵か味方かの判定(true:敵/false:味方)

    //行動関係
    protected int actionId; //行動ID
    protected boolean guardCheck; //防御判定
    protected StringBuilder actLog = new StringBuilder();
    protected int turnCount; //経過ターン数
    protected boolean paCheck; //PA発動中かのチェック
    protected boolean JACheck; //JA発動フラグ
    protected boolean skillCheck; //スキル発動フラグ

    //状態異常・変化関係
    protected boolean dead; //戦闘不能判定
    protected boolean attackPoison; //毒攻撃の判定
    protected boolean statePoison; //状態異常：毒の判定
    protected int poisonTurn; //状態異常：毒の経過ターン
    protected boolean stateStun; //状態異常；気絶の判定
    protected boolean stateBlind; //状態異常暗闇の判定
    protected int blindTurn; //状態異常：暗闇の経過ターン
    protected boolean stateSealed; //状態異常封印の判定
    protected int sealedTurn; //状態異常：封印の経過ターン


    public int getBattleHp() {
        return battleHp;
    }

    public void setBattleHp(int battleHp) {
        this.battleHp = battleHp;
    }

    public int getBattleStr() {
        return battleStr;
    }

    public void setBattleStr(int battleStr) {
        this.battleStr = battleStr;
    }

    public int getBattleDef() {
        return battleDef;
    }

    public void setBattleDef(int battleDef) {
        this.battleDef = battleDef;
    }

    public int getBattleDex() {
        return battleDex;
    }

    public void setBattleDex(int battleDex) {
        this.battleDex = battleDex;
    }

    public int getBattleAgi() {
        return battleAgi;
    }

    public void setBattleAgi(int battleAgi) {
        this.battleAgi = battleAgi;
    }

    public int getBattleLuck() {
        return battleLuck;
    }

    public void setBattleLuck(int battleLuck) {
        this.battleLuck = battleLuck;
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public StringBuilder getActLog() {
        return actLog;
    }

    public void setActLog(String actLog) {
        this.actLog.append(actLog);
    }

    public void setAttackPoison(boolean attackPoison) {
        this.attackPoison = attackPoison;
    }

    public void setStatePoison(boolean statePoison) {
        this.statePoison = statePoison;
    }

    public void setStateStun(boolean stateStun) {
        this.stateStun = stateStun;
    }

    public boolean isPaCheck() {
        return paCheck;
    }

    public void setPaCheck(boolean paCheck) {
        this.paCheck = paCheck;
    }

    public int getTurnCount() {
        return turnCount;
    }

    public boolean isJACheck() {
        return JACheck;
    }

    public void setJACheck(boolean JACheck) {
        this.JACheck = JACheck;
    }

    public boolean isSkillCheck() {
        return skillCheck;
    }

    public void setSkillCheck(boolean skillCheck) {
        this.skillCheck = skillCheck;
    }

    public boolean isDead() {
        return dead;
    }

    public boolean isEnemyFrag() {
        return enemyFrag;
    }

    public void setEnemyFrag(boolean enemyFrag) {
        this.enemyFrag = enemyFrag;
    }

    public boolean isStatePoison() {
        return statePoison;
    }

    public boolean isStateStun() {
        return stateStun;
    }

    public boolean isStateBlind() {
        return stateBlind;
    }

    //命中率
    public int getHitRate(){
        if(stateBlind){
            //状態異常暗闇の場合、半減
            return (int)((battleDex * 1.5) + (battleLuck * 0.5)) / 2;
        } else {
            return (int)((battleDex * 1.5) + (battleLuck * 0.5));
        }
    }

    //回避率
    public int getEvasionRate(){
        return (int)((battleAgi * 0.75) + (battleLuck * 0.25));
    }

    public void setStateBlind(boolean stateBlind) {
        this.stateBlind = stateBlind;
    }

    public boolean isStateSealed() {
        return stateSealed;
    }

    public void setStateSealed(boolean stateSealed) {
        this.stateSealed = stateSealed;
    }

    //Getter・Setterここまで

    //被ダメージ
    public void HitDamage(int damage){
        double lastDamage = damage;
        //防御中ならダメージ軽減
        if(guardCheck){
            lastDamage = lastDamage * GUARD_CONS;
        }
        battleHp = Math.max(battleHp - (int)lastDamage,0);
    }

    //戦闘不能の判定
    public void deadCheck(){
        if(battleHp <= 0){
            dead = true;
            actLog.append(getPlayerName() + "は倒れた…\n\n");
        }
    }

    //各種状態の初期化
    public void StateReset(){
        battleHp = defaultHp;
        battleStr = defaultStr;
        battleDef = defaultDef;
        battleAgi = defaultAgi;
        battleDex = defaultDex;
        battleLuck = defaultLuck;
        dead = false;
        guardCheck = false;
        attackPoison = false;
        statePoison = false;
        stateStun = false;
        stateBlind = false;
        poisonTurn = 0;
        paCheck = false;
        turnCount = 0;
        skillCheck = false;
        JACheck = false;
    }

    //現在の状態の確認
    public void StateUpdate(){

        //経過ターン数を増加
        turnCount++;

        //防御を解除
        guardCheck = false;

        //毒状態のチェック
        if(statePoison){
            if(poisonTurn < BAD_STATE_TURN){
                poisonTurn++;
                double poisonDamage = defaultHp * 0.05;
                //毒でHPが0にはならない
                if((int)poisonDamage > battleHp){
                    poisonDamage = battleHp - 1;
                }
                actLog.append(getPlayerName() + "は毒により" + (int)poisonDamage + "のダメージ\n\n");
                battleHp -= (int)poisonDamage;
            } else {
                //状態異常継続ターンを過ぎた場合、状態異常を解除
                statePoison = false;
                poisonTurn = 0;
            }
        }

        //暗闇状態のチェック
        if(stateBlind){
            if(blindTurn < BAD_STATE_TURN){
                blindTurn++;
            } else {
                //状態異常継続ターンを過ぎた場合、状態異常を解除
                stateBlind = false;
                blindTurn = 0;
            }
        }

        //封印状態のチェック
        if(stateSealed){
            if(sealedTurn < BAD_STATE_TURN){
                sealedTurn++;
            } else {
                //状態異常継続ターンを過ぎた場合、状態異常を解除
                stateSealed = false;
                sealedTurn = 0;
            }
        }

    }

    //actLogの初期化
    public void ActLogClear(){
        actLog.setLength(0);
    }

    //行動可能かどうか判定
    public boolean ActionPossible(){
        //戦闘不能or気絶中だと行動不可
        if(dead || stateStun){
            if(stateStun){
                actLog.append(getPlayerName() + "は気絶している。\n\n");
                //気絶の解除
                stateStun = false;
            }
            return false;
        } else {
            return true;
        }
    }

    //現在の状態を返す(true:状態異常あり/false:正常)
    public boolean isBadState(){
        if(dead || stateStun || stateSealed || stateBlind || statePoison){
            return true;
        } else {
            return  false;
        }
    }

    //状態異常をリセットする
    public void CureState(){
        statePoison = false;
        stateStun = false;
        stateBlind = false;
    }
}
