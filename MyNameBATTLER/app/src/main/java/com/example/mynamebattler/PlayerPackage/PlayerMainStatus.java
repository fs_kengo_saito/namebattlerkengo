package com.example.mynamebattler.PlayerPackage;

import com.example.mynamebattler.SkillPackage.Skill;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;

/*
　プレイヤー毎の基本となるステータスを管理するクラス
 */
public abstract class PlayerMainStatus {

    protected String playerName; //名前
    protected  int defaultHp; //体力
    protected  int defaultStr; //攻撃力
    protected  int defaultDef; //ディフェンス
    protected  int defaultDex; //器用さ
    protected  int defaultAgi; //速さ
    protected  int defaultLuck; //運
    protected int jobId; //ジョブID
    protected int paId; //PAID
    protected ArrayList<Skill> skillList;

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getDefaultHp() {
        return defaultHp;
    }

    public void setDefaultHp(int defaultHp) {
        this.defaultHp = defaultHp;
    }

    public int getDefaultStr() {
        return defaultStr;
    }

    public void setDefaultStr(int defaultStr) {
        this.defaultStr = defaultStr;
    }

    public int getDefaultDef() {
        return defaultDef;
    }

    public void setDefaultDef(int defaultDef) {
        this.defaultDef = defaultDef;
    }

    public int getDefaultDex() {
        return defaultDex;
    }

    public void setDefaultDex(int defaultDex) {
        this.defaultDex = defaultDex;
    }

    public int getDefaultAgi() {
        return defaultAgi;
    }

    public void setDefaultAgi(int defaultAgi) {
        this.defaultAgi = defaultAgi;
    }

    public int getDefaultLuck() {
        return defaultLuck;
    }

    public void setDefaultLuck(int defaultLuck) {
        this.defaultLuck = defaultLuck;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public int getPaId() {
        return paId;
    }

    public void setPaId(int paId) {
        this.paId = paId;
    }

    public ArrayList<Skill> getSkillList() {
        return skillList;
    }

    public void setSkillList(ArrayList<Skill> skillList) {
        this.skillList = skillList;
    }
}
