package com.example.mynamebattler.AdapterPackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.mynamebattler.R;

import java.util.ArrayList;

public class SkillListViewAdapter extends BaseAdapter {

    //使用するView
    static class SkillListViewHolder{
        TextView skillName;
        TextView skillCost;
        TextView skillEff;
    }

    private LayoutInflater inflater;
    private int itemLayoutId;
    private ArrayList<String> skillId,skillName,skillEff;
    private ArrayList<Integer> skillCost;

    //アダプターに値を格納
    public SkillListViewAdapter(Context context,int itemLayoutId,
                                ArrayList<String> skillId,
                                ArrayList<String> skillName,
                                ArrayList<Integer> skillCost,
                                ArrayList<String> skillEff){
        super();
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.itemLayoutId = itemLayoutId;
        this.skillId = skillId;
        this.skillName = skillName;
        this.skillCost = skillCost;
        this.skillEff = skillEff;
    }

    //TextViewと値を紐づけ
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        SkillListViewHolder holder;

        if(convertView == null){
            convertView = inflater.inflate(itemLayoutId,parent,false);
            holder = new SkillListViewHolder();
            holder.skillName = convertView.findViewById(R.id.ListItemSkillName);
            holder.skillCost = convertView.findViewById(R.id.ListItemSkillCost);
            holder.skillEff = convertView.findViewById(R.id.ListItemSkillEff);
            convertView.setTag(holder);
        } else {
            holder = (SkillListViewHolder)convertView.getTag();
        }

        holder.skillName.setText("【" + skillName.get(position) + "】");
        holder.skillCost.setText("消費SP： " + skillCost.get(position));
        holder.skillEff.setText("《効果》\n" + skillEff.get(position));

        return convertView;
    }

    @Override
    public int getCount(){
        return skillId.size();
    }

    @Override
    public Object getItem(int position){
        return null;
    }

    @Override
    public long getItemId(int position){
        return 0;
    }
}
