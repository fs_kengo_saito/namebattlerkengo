package com.example.mynamebattler.SkillPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.Calculation;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　ウィザード - スキル1
　ファイア
 */
public class Wizard_Skill_01 extends Skill implements Serializable {

    @Override
    public void Invoke(PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){
        actionMember.setActLog(actionMember.getPlayerName() + "は" + skillName + "を使用した！\n");
        if(myParty.getSp() < skillCost){
            //SPが足りない場合
            actionMember.setActLog("しかしSPが足らなかった…\n\n");
        } else {
            //スキルを使用した場合
            myParty.SPCountUpdate(-skillCost);

            //スキル：マジックチャージ使用中の場合(相手全体に攻撃
            if(actionMember.isSkillCheck()){
                actionMember.setActLog("溜めていた魔力を放出した！\n\n");
                for(PlayerData playerData : enemyParty.getParty()){
                    //攻撃時だけ相手の防御を0にする
                    playerData.setBattleDef(0);

                    //ダメージ計算(1.25倍)＆攻撃
                    double damage = Calculation.DamageCalculation(actionMember,playerData) * 1.25;
                    playerData.HitDamage((int) damage);
                    actionMember.setActLog(playerData.getPlayerName() +"へ防御無視の超強力な攻撃！\n" + (int)damage + "へダメージ！\n\n");

                    //相手の防御を元に戻す
                    playerData.setBattleDef(target.getDefaultDef());
                }
                actionMember.setSkillCheck(false);
            } else {
                //スキル：マジックチャージ未使用の場合はターゲットのみ
                //攻撃時だけ相手の防御を0にする
                target.setBattleDef(0);

                //ダメージ計算＆攻撃
                double damage = Calculation.DamageCalculation(actionMember,target);
                target.HitDamage((int) damage);
                actionMember.setActLog(target.getPlayerName() +"へ防御無視の強力な攻撃！\n" + (int)damage + "へダメージ！\n\n");

                //相手の防御を元に戻す
                target.setBattleDef(target.getDefaultDef());
            }
        }
    }
}
