package com.example.mynamebattler.ManagerPackage;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

import com.example.mynamebattler.Constant;
/*
　サウンドの管理をするクラス
 */
public class SoundManager implements Constant {

    private static final SoundManager INSTANCE = new SoundManager();
    private MediaPlayer mediaPlayer;
    private int seekPosition;
    private Context context;
    private String fileName;

    private SoundManager(){
        MediaPlayer mediaPlayer = new MediaPlayer();
    }

    public static SoundManager getInstance(){
        return INSTANCE;
    }

    //BGMの準備をする
    private void SetupBGM(Context context){

        mediaPlayer = new MediaPlayer();

        try{
            AssetFileDescriptor assetFileDescriptor = context.getAssets().openFd(fileName);
            mediaPlayer.setDataSource(assetFileDescriptor.getFileDescriptor(),
                    assetFileDescriptor.getStartOffset(),
                    assetFileDescriptor.getLength());
            mediaPlayer.setLooping(true); //ループ再生
            mediaPlayer.prepare();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    //タイトルBGMを再生する
    public void TitleBGMStart(Context context){
        this.context = context;
        //MediaPlayerが空ならセットアップ
        if(mediaPlayer == null){
            this.fileName = TITLE_BGM_FILE;
            SetupBGM(context);
        } else if(this.fileName == TITLE_BGM_FILE){
            BGMReStart();
        } else {
            //既に他の曲が選択中の場合、切り替え
            mediaPlayer.stop(); //停止
            mediaPlayer.reset(); //リセット
            mediaPlayer.release(); //解放
            mediaPlayer = null;
            this.fileName = TITLE_BGM_FILE;
            SetupBGM(context);
        }
    }

    //キャラクタークリエイトBGMを再生する
    public void CharacterCreateBGMStart(Context context){
        this.context = context;
        //MediaPlayerが空ならセットアップ
        if(mediaPlayer == null){
            this.fileName = CHARACTER_CREATE_BGM_FILE;
            SetupBGM(context);
        } else if(this.fileName == CHARACTER_CREATE_BGM_FILE){
            BGMReStart();
        } else {
            //既に他の曲が選択中の場合、切り替え
            mediaPlayer.stop(); //停止
            mediaPlayer.reset(); //リセット
            mediaPlayer.release(); //解放
            mediaPlayer = null;
            this.fileName = CHARACTER_CREATE_BGM_FILE;
            SetupBGM(context);
        }
    }

    //バトルBGMを再生する
    public void BattleBGMStart(Context context){
        this.context = context;
        //MediaPlayerが空ならセットアップ
        if(mediaPlayer == null){
            this.fileName = BATTLE_BGM_FILE;
            SetupBGM(context);
        } else if(this.fileName == BATTLE_BGM_FILE){
            BGMReStart();
        } else {
            //既に他の曲が選択中の場合、切り替え
            mediaPlayer.stop(); //停止
            mediaPlayer.reset(); //リセット
            mediaPlayer.release(); //解放
            mediaPlayer = null;
            this.fileName = BATTLE_BGM_FILE;
            SetupBGM(context);
        }
    }

    //リザルトBGMを再生する
    public void ResultBGMStart(Context context){
        this.context = context;
        //MediaPlayerが空ならセットアップ
        if(mediaPlayer == null){
            this.fileName = RESULT_BGM_FILE;
            SetupBGM(context);
        } else if(this.fileName == RESULT_BGM_FILE){
            BGMReStart();
        } else {
            //既に他の曲が選択中の場合、切り替え
            mediaPlayer.stop(); //停止
            mediaPlayer.reset(); //リセット
            mediaPlayer.release(); //解放
            mediaPlayer = null;
            this.fileName = RESULT_BGM_FILE;
            SetupBGM(context);
        }
    }


    //BGMを一時中断する
    public void BGMPause(){
        if(mediaPlayer == null) {
            seekPosition = 0;
        }
            mediaPlayer.pause();
            seekPosition = mediaPlayer.getCurrentPosition();
    }

    //BGMを再開する
    public void BGMReStart(){
        if(mediaPlayer == null){
            SetupBGM(context);
        }
        mediaPlayer.seekTo(seekPosition);
        mediaPlayer.start();
    }

    //BGMを完全に停止する
    public void BGMStop(){
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop(); //停止
            mediaPlayer.reset(); //リセット
            mediaPlayer.release(); //解放
        }
    }

}
