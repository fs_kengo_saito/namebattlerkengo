package com.example.mynamebattler.OrderPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

/*
　301
　ガンガン行こうぜ
　相手パーティーの1番目にいるキャラクターを攻撃
　攻撃率：100％、防御率：0％、スキル：使用できる状態なら即使用する
 */
public class Order301 extends BaseOrder {

    @Override
    public PlayerData getTarget(Party targetParty) {
        return targetParty.getParty().get(0);
    }

}
