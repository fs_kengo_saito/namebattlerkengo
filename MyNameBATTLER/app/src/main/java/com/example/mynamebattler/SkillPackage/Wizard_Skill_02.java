package com.example.mynamebattler.SkillPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.Calculation;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　ウィザード - スキル2
　マジックチャージ
 */
public class Wizard_Skill_02 extends Skill implements Serializable {

    @Override
    public void Invoke(PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){
        actionMember.setActLog(actionMember.getPlayerName() + "は" + skillName + "を使用した！\n");
        if(myParty.getSp() < skillCost){
            //SPが足りない場合
            actionMember.setActLog("しかしSPが足らなかった…\n\n");
        } else {
            //スキルを使用した場合
            myParty.SPCountUpdate(-skillCost);
            actionMember.setActLog(actionMember.getPlayerName() + "は魔力を溜めた！\n\n");

            //スキル使用フラグを立てる
            actionMember.setSkillCheck(true);
        }
    }
}
