package com.example.mynamebattler.PartyPackage;

import android.util.Log;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.OrderPackage.BaseOrder;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;
import java.util.LinkedList;
/*
　パーティークラスの基底クラス
 */
public class Party implements Constant, Serializable {

    protected int sp; //SP
    protected LinkedList<PlayerData> party; //パーティー
    protected PlayerData coveredTarget; //かばうを使用したキャラクター
    protected BaseOrder order; //作戦

    //コンストラクタ
    public Party(LinkedList<PlayerData> party){
        this.party = party;
    }

    //getter・setterここから

    public int getSp() {
        return sp;
    }

    public LinkedList<PlayerData> getParty() {
        return party;
    }

    public PlayerData getCoveredTarget() {
        return coveredTarget;
    }

    public void setCoveredTarget(PlayerData coveredTarget) {
        this.coveredTarget = coveredTarget;
    }

    public BaseOrder getOrder() {
        return order;
    }

    public void setOrder(BaseOrder order) {
        this.order = order;
    }

    //getter・setterここまで

    //SPを増減する
    public void SPCountUpdate(int sp){
        this.sp += sp;
        if(getSp() > SP_LIMIT){
            this.sp = SP_LIMIT;
        }
    }

    /*
    / メンバーの行動
    / BattleSceneActivityで選択した行動をPlayerのactIDに保存、このメソッドで取得して行動を決定する
    */
    public void MemberAction(PlayerData actionMember,PlayerData target, Party myParty, Party enemyParty,String skillId){
        int memberNumber = party.indexOf(actionMember); //行動するキャラがパーティーの何番目か検索

        //相手のパーティーにかばうを使用していた場合、ターゲットをかばうを使用しているキャラに変更
        if(enemyParty.getCoveredTarget() != null){
            if(enemyParty.getCoveredTarget().isDead()){
                //但し、戦闘不能場合はかばう状態を解除
                enemyParty.setCoveredTarget(null);
            } else {
                target = enemyParty.getCoveredTarget();
            }
        }

        //行動するキャラクターがかばうを使用しているキャラの場合又はかばうを使用しているキャラクターが戦闘不能の場合、かばうを解除
        if(actionMember == coveredTarget){
            coveredTarget = null;
        }

        //actIDからどの行動かを決定
        switch (actionMember.getActionId()){
                case ATTACK_ID:
                party.get(memberNumber).Attack(actionMember,target);
                SPCountUpdate(ATTACK_SP_INCREASE);
                break;
            case GUARD_ID:
                party.get(memberNumber).Guard(actionMember);
                SPCountUpdate(GUARD_SP_INCREASE);
                break;
            case SKILL_ID:
                party.get(memberNumber).Skill(skillId,target,actionMember,myParty,enemyParty);
                break;
            default:
                break;
        }
    }

}
