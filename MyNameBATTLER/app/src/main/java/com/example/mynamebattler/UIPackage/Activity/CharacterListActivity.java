package com.example.mynamebattler.UIPackage.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.mynamebattler.DBPackage.DBManager;
import com.example.mynamebattler.Constant;
import com.example.mynamebattler.ManagerPackage.CharacterDataManager;
import com.example.mynamebattler.ManagerPackage.ImageManager;
import com.example.mynamebattler.ManagerPackage.SoundManager;
import com.example.mynamebattler.R;
import com.example.mynamebattler.UIPackage.Fragment.CharacterListFragment;
import com.example.mynamebattler.UIPackage.Fragment.HeaderFragment;

import java.util.ArrayList;
/*
　キャラクター一覧画面の表示及びUI処理を行うクラス
 */
public class CharacterListActivity extends AppCompatActivity implements Constant {

    private ArrayList<Integer> allCharaId;
    private ArrayList<String> allCharaName,allCharaJob,allCharaPA;

    private CharacterDataManager characterDataManager;
    private SoundManager soundManager;
    private ImageManager imageManager;
    private Bundle bundle;
    private CharacterListFragment characterListFragment;
    private HeaderFragment headerFragment;

    //端末の戻るを禁止する
    @Override
    public void onBackPressed(){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.character_list_activity);

        //Bundleの初期化
        bundle = new Bundle();
        //各種インスタンスを取得
        characterDataManager = CharacterDataManager.getInstance();
        soundManager = SoundManager.getInstance();
        imageManager = ImageManager.getInstance();

        //キャラクタークリエイトBGMの再生
        soundManager.CharacterCreateBGMStart(this);

        InitHeaderFragment();
        InitCharacterListFragment();
        InitButton();

    }

    //Activityが隠れた時に呼び出し
    @Override
    protected void onPause(){
        super.onPause();
        soundManager.BGMPause();
    }

    //Activityが前面に表示された時に呼び出し
    protected void onResume(){
        super.onResume();
        //BGMの再開
        soundManager.BGMReStart();
    }

    //Activityが破棄されるタイミングでサービスを終了させる
    @Override
    protected void onDestroy(){
        super.onDestroy();
        soundManager.BGMStop();
    }

    //HeaderFragmentの表示
    private void InitHeaderFragment(){

        bundle.putString(BACK_ACT_CHECK_KEY,BACK_TITLE_ACT_KEY);
        headerFragment = new HeaderFragment();
        headerFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.headerFragmentFrame,headerFragment);
        transaction.commit();
    }

    //CharacterListFragmentの表示
    private void InitCharacterListFragment(){
        //DBから各種データを取得
        allCharaId = characterDataManager.GetAllCharacterID();
        allCharaName = characterDataManager.GetAllCharacterName();
        allCharaJob = characterDataManager.GetAllCharacterJob();
        allCharaPA = characterDataManager.GetAllCharacterPA();

        //バンドルにデータを格納
        bundle.putIntegerArrayList(CHARACTER_ID_LIST_KEY,allCharaId);
        bundle.putStringArrayList(CHARACTER_NAME_LIST_KEY,allCharaName);
        bundle.putStringArrayList(CHARACTER_JOB_LIST_KEY,allCharaJob);
        bundle.putStringArrayList(CHARACTER_PA_LIST_KEY,allCharaPA);

        //characterListFragmentを作成して表示
        characterListFragment = new CharacterListFragment();
        characterListFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.characterListFlame,characterListFragment);
        transaction.commit();
    }

    //ボタンの設定＆処理
    private void InitButton(){

        //キャラクター作成画面へ遷移
        ImageButton newCharacterBtn = findViewById(R.id.SendCharInitActButton);
        Bitmap btnImage2 = imageManager.GetImageBitmap(this,NEW_CHARACTER_BUTTON_IMAGE);
        newCharacterBtn.setImageBitmap(btnImage2);
        newCharacterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(),CharacterInitActivity.class);
                startActivity(intent);
            }
        });

    }

}
