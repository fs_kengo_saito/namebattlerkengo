package com.example.mynamebattler.SkillPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　ハンター - スキル1
　スプレッドショット
 */
public class Hunter_Skill_01 extends Skill implements Serializable {

    @Override
    public void Invoke(PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){
        actionMember.setActLog(actionMember.getPlayerName() + "は" + skillName + "を使用した！\n");
        if(myParty.getSp() < skillCost){
            //SPが足りない場合
            actionMember.setActLog("しかしSPが足らなかった…\n\n");
        } else {
            //スキルを使用した場合
            myParty.SPCountUpdate(-skillCost);
            actionMember.setActLog("矢が広範囲に乱れ打たれる！\n");

            //STRを0.6倍
            double str = actionMember.getBattleStr() * 0.6;
            actionMember.setBattleStr((int) str);

            //相手パーティー全体に順番に攻撃
            for (PlayerData enemy : enemyParty.getParty()) {
                actionMember.Attack(actionMember,enemy);
            }

            //STRを元に戻す
            actionMember.setBattleStr(actionMember.getDefaultStr());
        }
    }
}
