package com.example.mynamebattler.PartyPackage;

import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;
import java.util.LinkedList;
/*
　味方側のパーティークラス(Partyを継承)
 */
public class MyParty extends Party implements Serializable {

    public MyParty(LinkedList<PlayerData> party){
        super(party);
    }
}

