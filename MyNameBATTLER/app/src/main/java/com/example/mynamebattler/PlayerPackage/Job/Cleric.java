package com.example.mynamebattler.PlayerPackage.Job;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
職業：クレリック(Playerクラスを継承)
 */
public class Cleric extends PlayerData implements Constant, Serializable {

    public Cleric(String name){
        super(name);
    }

    //名前から取得したステータスでCharacter作成
    @Override
    protected void CharaCreate(){
        int randomIndex = (int)(Math.random()*10);
        defaultHp = (GetNumber(randomIndex,STATUS_RANGE_BASE) + STATUS_RANGE_D) * STATUS_HP_FACTOR;
        defaultStr = GetNumber(randomIndex+1,STATUS_RANGE_BASE) + STATUS_RANGE_B;
        defaultDef = GetNumber(randomIndex+2,STATUS_RANGE_BASE) + STATUS_RANGE_C;
        defaultAgi = GetNumber(randomIndex+3,STATUS_RANGE_BASE) + STATUS_RANGE_D;
        defaultDex = GetNumber(randomIndex+4,STATUS_RANGE_BASE) + STATUS_RANGE_E;
        defaultLuck = GetNumber(randomIndex+5,STATUS_RANGE_BASE) + STATUS_RANGE_A;
    }

    //JA用メソッド
    @Override
    public void JA(PlayerData actionMember, Party myParty, Party enemyParty){
        //自パーティーのSP60以上の場合、発動
        if(myParty.getSp() >= 60){
            setJACheck(true);
            actLog.append(getPlayerName() + "のJA発動！\nスキルの効果が味方全体化！\n\n");
        } else {
            setJACheck(false);
        }
    }

}
