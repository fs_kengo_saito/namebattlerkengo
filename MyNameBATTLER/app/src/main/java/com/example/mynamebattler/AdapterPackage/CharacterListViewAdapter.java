package com.example.mynamebattler.AdapterPackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.mynamebattler.R;

import java.util.ArrayList;
/*
　キャラクターリスト作成用のアダプタ
 */
public class CharacterListViewAdapter extends BaseAdapter {

    //使用するTextView
    static class CharacterListViewHolder{
        TextView charaName;
        TextView charaJob;
        TextView charaPA;
    }

    private LayoutInflater inflater;
    private int itemLayoutId;
    private ArrayList<Integer> charaId;
    private ArrayList<String> charaName,charaJob,charaPA;

    //アダプターに値を格納
    public CharacterListViewAdapter(Context context, int itemLayoutId,
                                    ArrayList<Integer> charaId,
                                    ArrayList<String> charaName,
                                    ArrayList<String> charaJob,
                                    ArrayList<String> charaPA){
        super();
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.itemLayoutId = itemLayoutId;
        this.charaId = charaId;
        this.charaName = charaName;
        this.charaJob = charaJob;
        this.charaPA = charaPA;
    }

    //TextViewと値を紐づけ
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        CharacterListViewHolder holder;

        if(convertView == null){
            convertView = inflater.inflate(itemLayoutId,parent,false);
            holder = new CharacterListViewHolder();
            holder.charaName = convertView.findViewById(R.id.ListItemCharacterName);
            holder.charaJob = convertView.findViewById(R.id.ListItemCharacterJob);
            holder.charaPA = convertView.findViewById(R.id.ListItemCharacterPA);
            convertView.setTag(holder);
        } else {
            holder = (CharacterListViewHolder)convertView.getTag();
        }

        holder.charaName.setText("Name： " + charaName.get(position));
        holder.charaJob.setText("職業： " + charaJob.get(position));
        holder.charaPA.setText("PA： " + charaPA.get(position));

        return convertView;
    }

    @Override
    public int getCount(){
        return charaId.size();
    }

    @Override
    public Object getItem(int position){
        return null;
    }

    @Override
    public long getItemId(int position){
        return 0;
    }

}
