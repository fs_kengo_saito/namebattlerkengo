package com.example.mynamebattler.OrderPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;
/*
　作戦データのベースクラス
 */
public abstract class BaseOrder {

    protected String orderName;
    protected int attackProbability; //攻撃確率
    protected int guardProbability; //防御確率
    protected boolean skillUseCheck; //スキル利用の有無

    //作戦名をセット
    public String getOrderName() {
        return orderName;
    }

    //作戦名を取得
    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    //ターゲッティングを設定
    public PlayerData getTarget(Party targetParty){
        return targetParty.getParty().get(0);
    }

    //攻撃をする確率をセット
    public void setAttackProbability(int attackProbability){
        this.attackProbability = attackProbability;
    }

    //攻撃をする確率を取得
    public int getAttackProbability(){
        return attackProbability;
    }

    //防御する確率をセット
    public void setGuardProbability(int guardProbability) {
        this.guardProbability = guardProbability;
    }

    //スキル使用フラグをセット
    public void setSkillUseCheck(int skillUseCheck) {
        if(skillUseCheck == 0){
            this.skillUseCheck = false;
        } else {
            this.skillUseCheck = true;
        }
    }

    //スキルを使用可能かどうかの判定(true：使用可能)
    public boolean getSkillUseCheck(Party party){
        return skillUseCheck;
    }

}
