package com.example.mynamebattler.PAPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　PA：205　スロースターター
　【発動条件】戦闘開始時
　STR＆DEF＆AGL＆DEX10％マイナス。4ターン目にSTR＆DEF＆AGL＆DEX20％アップ
 */
public class PA205_SLOW_STARTER implements BasePA,Serializable {

    @Override
    public void Invoke(PlayerData actionMember, Party myParty, Party enemyParty) {

        //発動条件判定
        if(actionMember.getTurnCount() == 1){
            //1ターン目
            actionMember.setActLog(actionMember.getPlayerName() + "のPA発動！\n" + actionMember.getPlayerName() + "はまだ本調子ではないようだ…\n\n");
            actionMember.setBattleStr(actionMember.getBattleStr() - (int)(actionMember.getDefaultStr() * 0.1));
            actionMember.setBattleDef(actionMember.getBattleDef() - (int)(actionMember.getDefaultDef() * 0.1));
            actionMember.setBattleAgi(actionMember.getBattleAgi() - (int)(actionMember.getDefaultAgi() * 0.1));
            actionMember.setBattleDex(actionMember.getBattleDex() - (int)(actionMember.getDefaultDex() * 0.1));
            actionMember.setPaCheck(true);
        } else if(actionMember.getTurnCount() == 4){
            //4ターン目
            actionMember.setActLog(actionMember.getPlayerName() + "のPA発動！\n" + actionMember.getPlayerName() + "の調子が上がった！\n\n");
            actionMember.setBattleStr(actionMember.getBattleStr() + (int)(actionMember.getDefaultStr() * 0.1) + (int)(actionMember.getDefaultStr() * 0.2));
            actionMember.setBattleDef(actionMember.getBattleDef() + (int)(actionMember.getDefaultDef() * 0.1) + (int)(actionMember.getDefaultDef() * 0.2));
            actionMember.setBattleAgi(actionMember.getBattleAgi() + (int)(actionMember.getDefaultAgi() * 0.1) + (int)(actionMember.getDefaultAgi() * 0.2));
            actionMember.setBattleDex(actionMember.getBattleDex() + (int)(actionMember.getDefaultDex() * 0.1) + (int)(actionMember.getDefaultDex() * 0.2));
            actionMember.setPaCheck(false);
        }
    }
}
