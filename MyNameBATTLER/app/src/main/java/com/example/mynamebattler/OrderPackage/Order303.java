package com.example.mynamebattler.OrderPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

/*
　303
　守りに徹しろ
　相手パーティーの3番目にいるキャラクターを攻撃
　攻撃率：0％、防御率：100％、スキル：SPが100未満では使用せず、SPが100以上で使用できる状態なら使用する
 */
public class Order303 extends BaseOrder {

    @Override
    public PlayerData getTarget(Party targetParty) {
        return targetParty.getParty().get(2);
    }

    @Override
    public boolean getSkillUseCheck(Party myParty) {
        if(myParty.getSp() < 100){
            return false;
        } else {
            return skillUseCheck;
        }
    }
}
