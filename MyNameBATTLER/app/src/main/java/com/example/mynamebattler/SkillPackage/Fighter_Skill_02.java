package com.example.mynamebattler.SkillPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　ファイター - スキル2
　シールドバッシュ
 */
public class Fighter_Skill_02 extends Skill implements Serializable {

    @Override
    public void Invoke(PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){
        actionMember.setActLog(actionMember.getPlayerName() + "は" + skillName + "を使用した！\n");
        if(myParty.getSp() < skillCost){
            //SPが足りない場合
            actionMember.setActLog("しかしSPが足らなかった…\n\n");
        } else {
            //スキルを使用した場合
            myParty.SPCountUpdate(-skillCost);
            actionMember.setActLog(target.getPlayerName() + "を盾で殴りつけた！\n");

            //STRを0.7倍にして攻撃
            double str = actionMember.getBattleStr() * 0.7;
            actionMember.setBattleStr((int) str);
            actionMember.Attack(actionMember,target);

            //STRを元に戻す
            actionMember.setBattleStr(actionMember.getDefaultStr());

            int stunCheck = (int)(Math.random()*100); //0～99の目標値を決定
            //50％の確率で相手を気絶(目標値が50以上なら気絶)
            if(stunCheck >= 50){
                target.setStateStun(true);
            }
        }
    }
}
