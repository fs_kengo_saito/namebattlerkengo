package com.example.mynamebattler.OrderPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

/*
　302
　堅実に戦え
　相手パーティーの2番目にいるキャラクターを攻撃
　攻撃率：50％、防御率：50％、スキル：SPが70未満では使用せず、SPが70以上で使用できる状態なら使用する
 */
public class Order302 extends BaseOrder {

    @Override
    public PlayerData getTarget(Party targetParty) {
        return targetParty.getParty().get(1);
    }

    @Override
    public boolean getSkillUseCheck(Party myParty) {
        if(myParty.getSp() < 70){
            return false;
        } else {
            return skillUseCheck;
        }
    }
}
