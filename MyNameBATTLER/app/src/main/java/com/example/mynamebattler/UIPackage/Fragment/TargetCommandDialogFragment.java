package com.example.mynamebattler.UIPackage.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.ManagerPackage.BattleManager;
import com.example.mynamebattler.R;

import java.util.ArrayList;
/*
　バトルコマンド - ターゲット選択ダイアログを管理するクラス
 */
public class TargetCommandDialogFragment extends DialogFragment implements Constant {

    private View view;

    private Bundle bundle;
    private String[] targetItems;

    private BattleManager battleManager;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){

        //各種インスタンスの取得
        battleManager = BattleManager.getInstance();

        //ダイアログビルダーを作成
        AlertDialog.Builder targetCommandBuilder = new AlertDialog.Builder(getActivity());

        //viewの取得
        view = LayoutInflater.from(getActivity()).inflate(R.layout.battle_command_fragment,null);

        GetListItems();

        targetCommandBuilder.setView(view)
                .setCustomTitle(GetCustomTitle(view))
                .setItems(targetItems, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        battleManager.setTargetId(which);
                        battleManager.ActionFixed(ATTACK_ID);
                    }
                })
                .setNegativeButton("閉じる",null);

        return targetCommandBuilder.create();
    }

    //ダイアログのタイトルを作成
    private TextView GetCustomTitle(View view){
        TextView customTitle = new TextView(view.getContext());
        customTitle.setText(R.string.TargetListTile); //表示テキスト
        customTitle.setGravity(Gravity.CENTER_HORIZONTAL); //中央揃え
        customTitle.setTextSize(24);
        customTitle.setTypeface(Typeface.DEFAULT_BOLD); //太字

        return customTitle;
    }

    private void GetListItems(){

        //Bundleから値を取得
        bundle = getArguments();
        //ターゲットの一覧を設定
        ArrayList<String> targets = bundle.getStringArrayList(ENEMY_LIST_KEY);
        targetItems = targets.toArray(new String[targets.size()]);

    }
}
