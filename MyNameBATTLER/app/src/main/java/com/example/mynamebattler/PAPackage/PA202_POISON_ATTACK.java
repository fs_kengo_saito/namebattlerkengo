package com.example.mynamebattler.PAPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　PA：202　毒の刃
　【発動条件】自パーティーのSP：30以上
　相手への攻撃時に状態異常：毒を付与
 */
public class PA202_POISON_ATTACK implements BasePA,Serializable {

    @Override
    public void Invoke(PlayerData actionMember, Party myParty, Party enemyParty) {
        //発動条件判定
        if(myParty.getSp() >= 50){
            actionMember.setActLog(actionMember.getPlayerName() + "のPA発動！\n攻撃時に毒を付与！\n\n");
            actionMember.setAttackPoison(true); //毒攻撃用のフラグをON
            actionMember.setPaCheck(true);
        } else {
            actionMember.setAttackPoison(false); //毒攻撃用のフラグをOFF
            actionMember.setPaCheck(false);
        }
    }
}
