package com.example.mynamebattler.PlayerPackage;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.ManagerPackage.PAManager;
import com.example.mynamebattler.ManagerPackage.SkillManager;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
/*
　プレイヤー情報を生成する基底クラス(
*/
public abstract class PlayerData extends PlayerBattleStatus implements PlayerAction,Constant,Serializable{

    protected int charaId; //キャラID

    //Getter・Setterここから
    public int getCharaId() {
        return charaId;
    }

    public void setCharaId(int charaId) {
        this.charaId = charaId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    //Getter・Setterここまで

    public PlayerData(String name){
        this.playerName = name;
        CharaCreate();
    }

    protected void CharaCreate(){
    }
    /*
    * 名前からハッシュ値を作成し、指定した位置の数値を取り出す
    * 引数…index:取り出す位置、max：最大値
    */
    protected int GetNumber(int index,int max){
        try{
            byte[] result = MessageDigest.getInstance("SHA-1").digest(this.playerName.getBytes());
            String digest = String.format("%040x",new BigInteger(1,result));

            String hex = digest.substring(index * 2,index * 2 + 2);

            int val = Integer.parseInt(hex,16);
            return val * max / 255;
        } catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

}
