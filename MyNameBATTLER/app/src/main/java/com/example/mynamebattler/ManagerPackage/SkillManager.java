package com.example.mynamebattler.ManagerPackage;

import android.util.Log;

import com.example.mynamebattler.DBPackage.DBManager;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;
import com.example.mynamebattler.SkillPackage.Cheater_Skill_02;
import com.example.mynamebattler.SkillPackage.Cleric_Skill_02;
import com.example.mynamebattler.SkillPackage.Fighter_Skill_02;
import com.example.mynamebattler.SkillPackage.Hunter_Skill_02;
import com.example.mynamebattler.SkillPackage.Knight_Skill_02;
import com.example.mynamebattler.SkillPackage.Skill;
import com.example.mynamebattler.SkillPackage.Cheater_Skill_01;
import com.example.mynamebattler.SkillPackage.Cleric_Skill_01;
import com.example.mynamebattler.SkillPackage.Fighter_Skill_01;
import com.example.mynamebattler.SkillPackage.Hunter_Skill_01;
import com.example.mynamebattler.SkillPackage.Knight_Skill_01;
import com.example.mynamebattler.SkillPackage.Warrior_Skill_01;
import com.example.mynamebattler.SkillPackage.Warrior_Skill_02;
import com.example.mynamebattler.SkillPackage.Wizard_Skill_01;
import com.example.mynamebattler.SkillPackage.Wizard_Skill_02;

import java.io.Serializable;
import java.util.ArrayList;

/*
　スキルを管理するクラス
 */
public class SkillManager implements Serializable {

    private DBManager dbManager;

    private static final SkillManager INSTANCE = new SkillManager();

    private SkillManager(){
        dbManager = DBManager.getInstance();
    }

    //シングルトンパターン
    public static SkillManager getInstance(){
        return INSTANCE;
    }

    private Skill skill;
    private ArrayList<String> skillInfo;

    //受け取ったスキルIDに応じたスキルを発動
    public void InvokeSkill(String skillId, PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){
        skillInfo = GetSkillInfo(skillId);

        //スキルIDに応じて使用するスキルを判定
        switch (skillId){
            case "FIG001":
                skill = new Fighter_Skill_01();
                SetSkillInfo();
                skill.Invoke(target,actionMember,myParty,enemyParty);
                break;
            case "FIG002":
                skill = new Fighter_Skill_02();
                SetSkillInfo();
                skill.Invoke(target,actionMember,myParty,enemyParty);
                break;
            case "WIZ001":
                skill = new Wizard_Skill_01();
                SetSkillInfo();
                skill.Invoke(target,actionMember,myParty,enemyParty);
                break;
            case "WIZ002":
                skill = new Wizard_Skill_02();
                SetSkillInfo();
                skill.Invoke(target,actionMember,myParty,enemyParty);
                break;
            case "CLR001":
                skill = new Cleric_Skill_01();
                SetSkillInfo();
                skill.Invoke(target,actionMember,myParty,enemyParty);
                break;
            case "CLR002":
                skill = new Cleric_Skill_02();
                SetSkillInfo();
                skill.Invoke(target,actionMember,myParty,enemyParty);
                break;
            case "KNI001":
                skill = new Knight_Skill_01();
                SetSkillInfo();
                skill.Invoke(target,actionMember,myParty,enemyParty);
                break;
            case "KNI002":
                skill = new Knight_Skill_02();
                SetSkillInfo();
                skill.Invoke(target,actionMember,myParty,enemyParty);
                break;
            case "HUN001":
                skill = new Hunter_Skill_01();
                SetSkillInfo();
                skill.Invoke(target,actionMember,myParty,enemyParty);
                break;
            case "HUN002":
                skill = new Hunter_Skill_02();
                SetSkillInfo();
                skill.Invoke(target,actionMember,myParty,enemyParty);
                break;
            case "WAR001":
                skill = new Warrior_Skill_01();
                SetSkillInfo();
                skill.Invoke(target,actionMember,myParty,enemyParty);
                break;
            case "WAR002":
                skill = new Warrior_Skill_02();
                SetSkillInfo();
                skill.Invoke(target,actionMember,myParty,enemyParty);
                break;
            case "CHE001":
                skill = new Cheater_Skill_01();
                SetSkillInfo();
                skill.Invoke(target,actionMember,myParty,enemyParty);
                break;
            case "CHE002":
                skill = new Cheater_Skill_02();
                SetSkillInfo();
                skill.Invoke(target,actionMember,myParty,enemyParty);
                break;
            default:
                Log.e("スキル使用エラー：","SkillManagerにスキルが設定されてません。");
                return;
        }
    }

    //スキル一覧を取得
    public ArrayList<Skill> GetSkillListItem(int jobId){
        dbManager = DBManager.getInstance();
        ArrayList<Skill> allSkill = new ArrayList<Skill>();
        ArrayList<String> jobInfo = dbManager.GetJobInfo(jobId);
        allSkill = dbManager.GetAllSkill(jobInfo.get(7));

        return allSkill;
    }

    //スキルIDからスキル情報を取得
    public ArrayList<String> GetSkillInfo(String skillId){
        dbManager = DBManager.getInstance();
        return dbManager.GetSkillInfo(skillId);
    }

    //スキル情報をセット
    private void SetSkillInfo(){
        skill.setSkillName(skillInfo.get(0));
        skill.setSkillCost(Integer.valueOf(skillInfo.get(1)));
        skill.setSkillEff(skillInfo.get(2));
    }

    //職業ごとのスキルグループを取得する
    public ArrayList<Skill> GetAllSkill(String skillGroupId){
        return dbManager.GetAllSkill(skillGroupId);
    }

}
