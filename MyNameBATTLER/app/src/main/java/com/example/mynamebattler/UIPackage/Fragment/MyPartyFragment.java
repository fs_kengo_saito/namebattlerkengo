package com.example.mynamebattler.UIPackage.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.ManagerPackage.JobManager;
import com.example.mynamebattler.ManagerPackage.PAManager;
import com.example.mynamebattler.PlayerPackage.PlayerData;
import com.example.mynamebattler.R;

import java.util.ArrayList;
/*
　MyPartyFragmentを管理するクラス
 */
public class MyPartyFragment extends Fragment implements Constant {

    private Bundle bundle;
    private TextView member1NameText,member2NameText,member3NameText;
    private TextView member1StateText, member2StateText, member3StateText;
    private TextView member1JobPAText, member2JobPAText, member3JobPAText;
    private TextView member1HPText,member2HPText,member3HPText;
    private CardView member1CardView,member2CardView,member3CardView;
    private PlayerData member1,member2,member3;
    private JobManager jobManager;
    private PAManager paManager;

    //Viewの生成(Fragment更新時に必ず更新される様に既存のViewをremove後、設定する)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        super.onCreateView(inflater,container,savedInstanceState);
        container.removeAllViews();
        View rootView = inflater.inflate(R.layout.party_fragment,container,false);

        //各Managerのインスタンスを取得
        jobManager = JobManager.getInstance();
        paManager = PAManager.getInstance();

        return rootView;

    }

    //View生成後の処理
    @Override
    public void onViewCreated(View view,Bundle savedInstanceState){
        super.onViewCreated(view,savedInstanceState);

        //Bundleから値を取得
        bundle = getArguments();
        member1 = (PlayerData) bundle.getSerializable(MYPARTY_PLAYER1_KEY);
        member2 = (PlayerData) bundle.getSerializable(MYPARTY_PLAYER2_KEY);
        member3 = (PlayerData) bundle.getSerializable(MYPARTY_PLAYER3_KEY);

        InitBackColor(view);
        InitTextUI(view);

    }

    //背景色の設定
    private void InitBackColor(View view){
        member1CardView = view.findViewById(R.id.member1CardView);
        member2CardView = view.findViewById(R.id.member2CardView);
        member3CardView = view.findViewById(R.id.member3CardView);

        member1CardView.setCardBackgroundColor(Color.parseColor(MY_PARTY_BACK_COLOR));
        member2CardView.setCardBackgroundColor(Color.parseColor(MY_PARTY_BACK_COLOR));
        member3CardView.setCardBackgroundColor(Color.parseColor(MY_PARTY_BACK_COLOR));
    }

    //Bundleから取得した情報を基にテキストを設定
    private void InitTextUI(View view){

        //各メンバーが存在するかどうかで処理を分ける(キャラが無い場合は未選択とする)
        if(member1 != null){
            InitMember1Text(view,true);
        } else {
            InitMember1Text(view,false);
        }

        if(member2 != null){
            InitMember2Text(view,true);
        } else {
            InitMember2Text(view,false);
        }

        if(member3 != null){
            InitMember3Text(view,true);
        } else {
            InitMember3Text(view,false);
        }

    }

    //メンバー1のテキストをセット　引数…true:キャラあり、false：キャラ無し
    private void InitMember1Text(View view,boolean charaNullCheck){

        member1NameText = view.findViewById(R.id.member1NameText);
        member1StateText = view.findViewById(R.id.member1StateText);
        member1JobPAText = view.findViewById(R.id.member1JobPANameText);
        member1HPText = view.findViewById(R.id.member1HPText);

        if(charaNullCheck){
            ArrayList<String> jobInfo = jobManager.GetJobInfo(member1.getJobId());
            ArrayList<String> PAInfo = paManager.GetPAInfo(member1.getPaId());
            member1NameText.setText(member1.getPlayerName());
            member1JobPAText.setText("〈JOB/PA〉\n" + jobInfo.get(0) +"/" + PAInfo.get(0));
            member1HPText.setText("〈HP〉\n" + member1.getBattleHp() + "/" + member1.getDefaultHp());
            setStateText(member1,member1StateText);
        } else {
            member1NameText.setText(R.string.PartyNull);
            member1StateText.setText(null);
            member1JobPAText.setText(null);
            member1HPText.setText(null);
        }
    }

    //メンバー2のテキストをセット　引数…true:キャラあり、false：キャラ無し
    private void InitMember2Text(View view,boolean charaNullCheck){

        member2NameText = view.findViewById(R.id.member2NameText);
        member2StateText = view.findViewById(R.id.member2StateText);
        member2JobPAText = view.findViewById(R.id.member2JobPANameText);
        member2HPText = view.findViewById(R.id.member2HPText);

        if(charaNullCheck){
            ArrayList<String> jobInfo = jobManager.GetJobInfo(member2.getJobId());
            ArrayList<String> PAInfo = paManager.GetPAInfo(member2.getPaId());
            member2NameText.setText(member2.getPlayerName());
            member2JobPAText.setText("〈JOB/PA〉\n" + jobInfo.get(0) +"/" + PAInfo.get(0));
            member2HPText.setText("〈HP〉\n" + member2.getBattleHp() + "/" + member2.getDefaultHp());
            setStateText(member2,member2StateText);
        } else {
            member2NameText.setText(R.string.PartyNull);
            member2StateText.setText(null);
            member2JobPAText.setText(null);
            member2HPText.setText(null);
        }
    }

    //メンバー2のテキストをセット　引数…true:キャラあり、false：キャラ無し
    private void InitMember3Text(View view,boolean charaNullCheck){

        member3NameText = view.findViewById(R.id.member3NameText);
        member3StateText = view.findViewById(R.id.member3StateText);
        member3JobPAText = view.findViewById(R.id.member3JobPANameText);
        member3HPText = view.findViewById(R.id.member3HPText);

        if(charaNullCheck){
            ArrayList<String> jobInfo = jobManager.GetJobInfo(member3.getJobId());
            ArrayList<String> PAInfo = paManager.GetPAInfo(member3.getPaId());
            member3NameText.setText(member3.getPlayerName());
            member3JobPAText.setText("〈JOB/PA〉\n" + jobInfo.get(0) +"/" + PAInfo.get(0));
            member3HPText.setText("〈HP〉\n" + member3.getBattleHp() + "/" + member3.getDefaultHp());
            setStateText(member3,member3StateText);
        } else {
            member3NameText.setText(R.string.PartyNull);
            member3StateText.setText(null);
            member3JobPAText.setText(null);
            member3HPText.setText(null);
        }
    }

    //ステータス情報テキストをセット
    private void setStateText(PlayerData member,TextView stateText){
        if(member.isBadState()){
            stateText.setText("〈状態〉\n");
            if(member.isDead()){
                stateText.setText("戦闘不能 ");
            }
            if(member.isStateSealed()){
                stateText.setText("封印 ");
            }
            if(member.isStateBlind()){
                stateText.setText("暗闇 ");
            }
            if(member.isStatePoison()){
                stateText.setText("毒 ");
            }
            if(member.isStateStun()){
                stateText.setText("気絶 ");
            }
        } else {
            stateText.setText("〈状態〉\n正常");
        }
    }
}
