package com.example.mynamebattler.ManagerPackage;

import android.os.Handler;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.PartyPackage.EnemyParty;
import com.example.mynamebattler.PartyPackage.MyParty;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;
import com.example.mynamebattler.SkillPackage.Skill;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

/*
　バトル進行を管理するクラス
 */
public class BattleManager implements Constant {

    private static final BattleManager INSTANCE = new BattleManager();

    private BattleManager(){

    }

    //シングルトンパターン
    public static BattleManager getInstance(){
        return INSTANCE;
    }

    private int turnCount,actionMemberCount;
    private int targetId;
    private String skillId;
    private  boolean myPartyWin,enemyPartyWin;
    private PlayerData actionCharacter,nextActionCharacter;
    private Party myParty,enemyParty,actParty,notActParty;
    private LinkedList<PlayerData> allMember = new LinkedList<PlayerData>();
    private StringBuilder battleLog = new StringBuilder();
    private PartyManager partyManager;
    private OrderManager orderManager;
    private boolean waitFrag, abilityCheckEnd,autoBattleFrag,usedSkillFrag;
    private Handler handler = new Handler();

    //getter・setterここから

    public boolean isMyPartyWin() {
        return myPartyWin;
    }

    public boolean isEnemyPartyWin() {
        return enemyPartyWin;
    }

    public StringBuilder getBattleLog() {
        return battleLog;
    }

    public void setBattleLog(String battleLog) {
        this.battleLog.append(battleLog);
    }

    public Party getMyParty() {
        return myParty;
    }

    public Party getEnemyParty() {
        return enemyParty;
    }

    public void setMyParty(Party myParty) {
        this.myParty = myParty;
    }

    public void setEnemyParty(Party enemyParty) {
        this.enemyParty = enemyParty;
    }

    public PlayerData getActionCharacter() {
        return actionCharacter;
    }

    public String getNextActionCharacterName() {
        if(nextActionCharacter == null){
            return "-----";
        }
        return nextActionCharacter.getPlayerName();
    }

    public boolean isWaitFrag() {
        return waitFrag;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    public void setSkillId(String skillId) {
        this.skillId = skillId;
    }

    public boolean isAutoBattleFrag() {
        return autoBattleFrag;
    }

    public void setUsedSkillFrag(boolean usedSkillFrag) {
        this.usedSkillFrag = usedSkillFrag;
    }

    //getter・setterここまで

    //バトル開始(各種データを初期化する)
    public void battleStart(){
        //各マネージャーのインスタンスを取得
        partyManager = PartyManager.getInstance();
        orderManager = OrderManager.getInstance();

        //各パーティーの準備
        myParty = new MyParty(partyManager.getMyParty()); //自分のパーティーを取得
        setMyParty(myParty);
        enemyParty = new EnemyParty(partyManager.getEnemyParty()); //敵のパーティーを取得
        setEnemyParty(enemyParty);

        //戦闘に参加している全キャラをallMemberリストに格納
        allMember.clear();
        for(int i =0;i < partyManager.getMyParty().size();i++){
            allMember.add(partyManager.getMyParty().get(i));
            allMember.add(partyManager.getEnemyParty().get(i));
        }

        //全キャラの情報を初期化
        for(PlayerData playerData : allMember){
            playerData.StateReset();
        }

        //各変数値を初期化
        turnCount = 1; //ターン数を0
        myPartyWin = false; //味方勝利判定をOFF
        enemyPartyWin = false; //敵側勝利判定をOFF
        myParty.SPCountUpdate(SP_DEFAULT); //味方のSPを初期化
        enemyParty.SPCountUpdate(SP_DEFAULT); //敵のSPを初期化
        battleLog.setLength(0); //バトルログを初期化
        actionMemberCount = 0; //行動するメンバーのカウントを初期化
        AgiCheck(); //行動順を決定
        actionCharacter = allMember.get(actionMemberCount); //行動するキャラクターの取得
        nextActionCharacter = allMember.get(actionMemberCount + 1); //次に行動するキャラクターを取得
        waitFrag = true; //ターン開始まで待機
        autoBattleFrag = false; //オートモードでは始めない
        usedSkillFrag = false; //スキル使用フラグをOFF
        orderManager.setOrder(myParty,301); //自パーティーの作戦初期値は301
        orderManager.setOrder(enemyParty,305); //敵パーティーの作戦初期値は305

        setBattleLog("～バトルスタート！～\n\n"); //バトルログに開始文言をセット

        AbilityCheck(); //アビリティチェック

        //2秒間待機して行動開始
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ActionCharacterCheck(); //行動するキャラクターを判定
            }
        },2000);
    }

    //行動を確定して実行する
    public void ActionFixed(int actonId){

        actionCharacter.setActionId(actonId);//選択したアクションIDをセット

        Action(allMember.get(actionMemberCount)); //行動

        BattleEndCheck(); //バトル終了の判定

        NextPhase(); //次のフェーズを判定

    }

    //次のフェーズ(キャラ1回分の行動)を決定
    public void NextPhase(){

        actionMemberCount++; //行動人数を1増やす

        //現ターン内の行動人数に応じて処理を分ける
        if(actionMemberCount >= allMember.size()){
            //全てのキャラが行動し終えた場合
            NextTurn(); //次のターンに移行
        } else {
            //まだ未行動のキャラがいる場合
            actionCharacter = allMember.get(actionMemberCount); //行動するキャラクターの取得

            //次に行動するキャラクターを取得
            if(actionMemberCount == allMember.size() - 1){
                //行動するキャラが現ターン内の最後場合
                nextActionCharacter = null; //次の行動キャラは無し
            } else {
                //行動するキャラが現ターン内の最後以外
                nextActionCharacter = allMember.get(actionMemberCount + 1); //次に行動するキャラクターを取得
            }

            //行動するキャラクターが敵かどうか判定
            ActionCharacterCheck();
        }
    }

    //ターンのカウント処理
    private void NextTurn(){
        turnCount++; //ターンカウントを1増やす
        AllMemberUpdate(); //AllMemberの状態を更新
        AgiCheck(); //行動順を決定
        actionMemberCount = 0; //行動するメンバーのカウントを初期化
        actionCharacter = allMember.get(actionMemberCount); //行動するキャラクターの取得
        nextActionCharacter = allMember.get(actionMemberCount + 1); //次に行動するキャラクターを取得
        setBattleLog("-------" + turnCount + "ターン目-------\n\n");
        AllCharacterStateUpdate(); //各キャラクターステータスの更新
        AbilityCheck(); //アビリティチェック
        //2秒間待機して行動開始
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ActionCharacterCheck(); //行動するキャラクターを判定
            }
        },2000);

    }

    //行動するキャラクターを判定
    private void ActionCharacterCheck(){
        //行動するキャラクターが行動可能かチェック
        if(actionCharacter.ActionPossible()){
            //行動可能な場合
            actParty = ActionParty(actionCharacter); //行動するのがどちらのパーティーか判定
            setBattleLog(actionCharacter.getPlayerName() + "　の番\n");
            if(actionCharacter.isEnemyFrag()){
                //敵の行動の場合、2秒間待機して敵の行動を実行
                waitFrag = true;
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AutoAction();
                    }
                },2000);
            } else {
                //味方の行動の場合
                //オートモードのフラグがtrueなら2秒待機してオート実行
                if(autoBattleFrag){
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AutoAction();
                        }
                    },2000);
                } else {
                    waitFrag = false;
                }
            }
        } else {
            //行動不可の場合
            NextPhase();
        }
    }

    //AllMemberの状態を最新の情報に更新
    private void AllMemberUpdate(){

        //allMemberを一旦クリア
        allMember.clear();

        //戦闘に参加している全キャラをリストに格納
        for(int i =0;i < partyManager.getMyParty().size();i++){
            allMember.add(partyManager.getMyParty().get(i));
            allMember.add(partyManager.getEnemyParty().get(i));
        }
    }

    //行動順をAGIの高い順に決定
    private void AgiCheck(){
        //ComparatorでパーティーをAGI順に並び替え
        Collections.sort(allMember, new Comparator<PlayerData>() {
            @Override
            public int compare(PlayerData o1, PlayerData o2) {
                return o2.getBattleAgi() - o1.getBattleAgi();
            }
        });
    }

    //PA・JA発動チェック
    private void AbilityCheck(){
        abilityCheckEnd = false;

        for(PlayerData member:allMember){
            //戦闘不能で無ければアビリティをチェック
            if(!member.isDead()){
                actParty = ActionParty(member);
                member.JA(member,actParty,notActParty);
                member.PA(member,actParty,notActParty);
                setBattleLog(member.getActLog().toString());
                member.ActLogClear();
            }
            //全員のチェックが完了を通知
            if(allMember == null){
                abilityCheckEnd = true;
            }
        }

    }

    //全キャラクターのステータスを更新
    private void AllCharacterStateUpdate(){
        for(PlayerData member : allMember){
            member.StateUpdate();
        }
    }

    //行動
    private void Action(PlayerData playerData){
        actParty.MemberAction(actionCharacter,Targeting(),actParty,notActParty,skillId); //行動
        setBattleLog(playerData.getActLog().toString()); //キャラの行動ログをバトルログにセット
        playerData.ActLogClear(); //キャラの行動ログをクリア
    }

    //行動するパーティーとターゲットを決定
    private Party ActionParty(PlayerData member){
        Party party;

        //行動するキャラが敵か味方か確認し、行動するパーティーを決定する
        if(member.isEnemyFrag()){
            //行動するのが敵側
            party = enemyParty;
            notActParty = myParty;
         } else {
            //行動するのが味方側
            party = myParty;
            notActParty = enemyParty;
        }
        return party;
    }

    //ターゲットを決定する
    private PlayerData Targeting(){
        PlayerData target;

        //オートモード・行動するのが敵・スキル使用の何れか場合は作戦に応じたターゲッティングを実行
        if(autoBattleFrag || actionCharacter.isEnemyFrag() || usedSkillFrag){
            target = orderManager.getTarget(actParty,notActParty);
            usedSkillFrag = false;
        } else {
            //自キャラの手動モード時は選択したターゲットを指定
            target = notActParty.getParty().get(targetId); //選択した相手をターゲットIDから取得
        }

        //ターゲットが戦闘不能の場合はパーティーの最も先頭にいる生存キャラをターゲット
        if(target.isDead()){
            for(PlayerData playerData : notActParty.getParty()){
                if(!playerData.isDead()){
                    target = playerData;
                    break;
                }
            }
        }

        return target;
    }

    //バトル終了かどうか判定
    private void BattleEndCheck(){
        //味方パーティーの戦闘不能人数をカウント
        int myPartyDeadCount = 0;
        int enemyPartyDeadCount = 0;
        for(PlayerData playerData :myParty.getParty()){
            if(playerData.isDead()){
                myPartyDeadCount++;
            }
        }

        //味方パーティー全員が戦闘不能ならエネミー側の勝利
        if(myPartyDeadCount == myParty.getParty().size()){
            enemyPartyWin = true;
            setBattleLog("～戦闘終了！～\n\n"); //バトルログに開始文言をセット
        }

        //エネミーパーティーの戦闘不能人数をカウント
        for(PlayerData playerData :enemyParty.getParty()){
            if(playerData.isDead()){
                enemyPartyDeadCount++;
            }
        }

        //全員が戦闘不能なら味方側の勝利
        if(enemyPartyDeadCount == enemyParty.getParty().size()){
            myPartyWin = true;
            setBattleLog("～戦闘終了！～\n\n"); //バトルログに開始文言をセット
        }

    }

    //オートモードの切り替え
    public void AutoModeSwitch(){
        if(autoBattleFrag){
            autoBattleFrag = false;
        } else {
            autoBattleFrag = true;
            AutoAction();
        }
    }

    //敵の行動及びオートモード時の味方の行動
    public void AutoAction(){
        int actionId = orderManager.getAction(myParty);
        if(actionId == SKILL_ID){
            //使用するのがスキルの場合、スキルを使用できるSPがあるかチェック
            //スキルコスト順に並び替え
            ArrayList<Skill> skillList = actionCharacter.getSkillList();
            Collections.sort(skillList, new Comparator<Skill>() {
                @Override
                public int compare(Skill o1, Skill o2) {
                    return o2.getSkillCost() - o1.getSkillCost();
                }
            });

            if(skillList.get(0).getSkillCost() <= actParty.getSp()){
               //使用できるSPがあればランダムに選択し、スキルIDをセット
                int randomUseSkill = (int)(Math.random()*skillList.size());
                skillId = actionCharacter.getSkillList().get(randomUseSkill).getSkillId();
            } else {
                //スキルを使用するSPが無ければ通常攻撃に変更
                actionId = ATTACK_ID;
            }
        }
        ActionFixed(actionId);
    }

}
