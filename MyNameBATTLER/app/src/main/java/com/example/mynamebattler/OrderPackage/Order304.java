package com.example.mynamebattler.OrderPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

/*
　304
　SPを溜めろ
　相手パーティーで最も残HPが低いキャラクターを攻撃
　攻撃率：30％、防御率：70％、スキル：使用しない
 */
public class Order304 extends BaseOrder {

    @Override
    public PlayerData getTarget(Party targetParty) {
        //残り体力が少ない順に並び替え
        LinkedList<PlayerData> list = targetParty.getParty();
        Collections.sort(list, new Comparator<PlayerData>() {
            @Override
            public int compare(PlayerData o1, PlayerData o2) {
                return o1.getBattleHp() - o2.getBattleHp();
            }
        });
        //最前(一番残体力が多い)のキャラを返す
        return list.getFirst();
    }

}
