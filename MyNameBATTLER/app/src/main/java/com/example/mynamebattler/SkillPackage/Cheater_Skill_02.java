package com.example.mynamebattler.SkillPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　転生者 - スキル2
　スキル封じ
 */
public class Cheater_Skill_02 extends Skill implements Serializable {

    @Override
    public void Invoke(PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){
        actionMember.setActLog(actionMember.getPlayerName() + "は" + skillName + "を使用した！\n");
        if(myParty.getSp() < skillCost){
            //SPが足りない場合
            actionMember.setActLog("しかしSPが足らなかった…\n\n");
        } else {
            //スキルを使用した場合
            myParty.SPCountUpdate(-skillCost);
            actionMember.setActLog("相手のスキル使用を禁止する！\n");

            //STRを0.5倍
            double str = actionMember.getBattleStr() * 0.5;
            actionMember.setBattleStr((int) str);

            //攻撃
            actionMember.Attack(actionMember,target);

            //封印を付与
            target.setStateSealed(true);
            actionMember.setActLog(target.getPlayerName() + "はスキルを封印された！\n");

            //STRを元に戻す
            actionMember.setBattleStr(actionMember.getDefaultStr());
        }
    }
}
