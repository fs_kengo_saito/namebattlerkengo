package com.example.mynamebattler.UIPackage.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.example.mynamebattler.AdapterPackage.CharacterListViewAdapter;
import com.example.mynamebattler.Constant;
import com.example.mynamebattler.R;
import com.example.mynamebattler.UIPackage.Activity.CharacterInfoActivity;

import java.util.ArrayList;
/*
　CharacterListFragmentを管理するクラス
 */
public class CharacterListFragment extends Fragment implements Constant, AdapterView.OnItemClickListener {

    private Bundle bundle;
    private ArrayList<Integer> allCharaId;
    private ArrayList<String> allCharaName,allCharaJob,allCharaPA;

    //Viewの生成(Fragment更新時に必ず更新される様に既存のViewをremove後、設定する)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        super.onCreateView(inflater,container,savedInstanceState);
        container.removeAllViews();
        View rootView = inflater.inflate(R.layout.chararacter_list_fragment,container,false);
        return rootView;

    }

    //View生成後の処理
    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        InitCharacterList(view);
    }

    //リストの表示
    private void InitCharacterList(View view){
        //バンドルから各種データを取得
        bundle = getArguments();
        allCharaId = bundle.getIntegerArrayList(CHARACTER_ID_LIST_KEY);
        allCharaName = bundle.getStringArrayList(CHARACTER_NAME_LIST_KEY);
        allCharaJob = bundle.getStringArrayList(CHARACTER_JOB_LIST_KEY);
        allCharaPA = bundle.getStringArrayList(CHARACTER_PA_LIST_KEY);

        //ListViewの設定
        ListView characterList = view.findViewById(R.id.CharacterList);
        BaseAdapter adapter = new CharacterListViewAdapter(view.getContext(),R.layout.character_list_item,
                allCharaId,
                allCharaName,
                allCharaJob,
                allCharaPA);
        characterList.setAdapter(adapter);
        characterList.setOnItemClickListener(this);
    }

    //リストを押下時の動作(押下したキャラクター情報画面へ遷移)
    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id){
        Intent intent = new Intent(v.getContext(),CharacterInfoActivity.class);
        intent.putExtra(_CHARA_ID,allCharaId.get(position));
        startActivity(intent);
    }
}
