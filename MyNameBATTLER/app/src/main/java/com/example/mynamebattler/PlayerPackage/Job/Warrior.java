package com.example.mynamebattler.PlayerPackage.Job;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.Calculation;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
職業：ウォーリアー(Playerクラスを継承)
 */
public class Warrior extends PlayerData implements Constant, Serializable {

    public Warrior(String name){
        super(name);
    }

    //名前から取得したステータスでCharacter作成
    @Override
    protected void CharaCreate(){
        int randomIndex = (int)(Math.random()*10);
        defaultHp = (GetNumber(randomIndex,STATUS_RANGE_BASE) + STATUS_RANGE_A) * STATUS_HP_FACTOR;
        defaultStr = GetNumber(randomIndex+1,STATUS_RANGE_BASE) + STATUS_RANGE_A;
        defaultDef = GetNumber(randomIndex+2,STATUS_RANGE_BASE) + STATUS_RANGE_D;
        defaultAgi = GetNumber(randomIndex+3,STATUS_RANGE_BASE) + STATUS_RANGE_D;
        defaultAgi = GetNumber(randomIndex+4,STATUS_RANGE_BASE) + STATUS_RANGE_E;
        defaultLuck = GetNumber(randomIndex+5,STATUS_RANGE_BASE) + STATUS_RANGE_E;
    }

    //攻撃用メソッド
    public void Attack(PlayerData target){
        guardCheck = false;
        int damage = Calculation.DamageCalculation(this,target);
        actLog.append(getPlayerName() + "の攻撃！\n");
        target.HitDamage(damage);
        //前のターンでスキルを使用していた場合、DEF/AGIをデフォルト値に戻す
        if(isSkillCheck()){
            setSkillCheck(false);
            setBattleDef(defaultDef);
            setBattleAgi(defaultAgi);
        }
        actLog.append(target.getPlayerName() + "に" + damage +"のダメージ\n\n");
    }

    //防御用メソッド
    public void Guard(){
        guardCheck = true;
        //前のターンでスキルを使用していた場合、DEF/AGIをデフォルト値に戻す
        if(isSkillCheck()){
            setSkillCheck(false);
            setBattleDef(defaultDef);
            setBattleAgi(defaultAgi);
        }
        actLog.append(getPlayerName() + "は防御した！\n\n");
    }

    //JA
    @Override
    public void JA(PlayerData actionMember, Party myParty, Party enemyParty){

        //HPの1割を判定基準とする
        int checkHP = defaultHp / 10;

        //現在のHPが判定基準未満の場合、JA発動
        if(battleHp < checkHP){
            //JA未発動の状態
            if(!JACheck){
                //STRを1.5倍
                double str = battleStr * 1.5;
                setBattleStr((int)str);
                actLog.append(getPlayerName() + "のJA発動！\n攻撃力が大幅アップ！\n\n");
                setJACheck(true);
            }
        } else {
            setBattleStr(defaultStr);
            setJACheck(false);
        }
    }

}
