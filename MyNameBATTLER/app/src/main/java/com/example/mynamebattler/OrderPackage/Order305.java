package com.example.mynamebattler.OrderPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

/*
　305
　バランスよく戦え
　相手パーティーで最も残HPが多いキャラクターを攻撃
　攻撃率：70％、防御率：30％、スキル：SPが50未満では使用せず、SPが50以上で使用できる状態なら使用する
 */
public class Order305 extends BaseOrder {

    @Override
    public PlayerData getTarget(Party targetParty) {
        //残り体力が少ない順に並び替え
        LinkedList<PlayerData> list = targetParty.getParty();
        Collections.sort(list, new Comparator<PlayerData>() {
            @Override
            public int compare(PlayerData o1, PlayerData o2) {
                return o1.getBattleHp() - o2.getBattleHp();
            }
        });
        //最後尾(一番残体力が多い)のキャラを返す
        return list.getLast();
    }

    @Override
    public boolean getSkillUseCheck(Party myParty) {
        if(myParty.getSp() < 50){
            return false;
        } else {
            return skillUseCheck;
        }
    }
}
