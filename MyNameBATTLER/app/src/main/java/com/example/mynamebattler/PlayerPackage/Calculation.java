package com.example.mynamebattler.PlayerPackage;

import com.example.mynamebattler.Constant;

import java.io.Serializable;
/*
　ダメージなどのゲーム中に使用する値を計算するクラス
 */
public class Calculation implements Serializable, Constant {

    /*
　   与ダメ計算
　   式：(自STR × STR定数  × (1 + (自DEX + 自AGI) / 1000) - (敵DEF × DEF定数 ×(1 + 敵LUCK / 1000))
    */
    public static int DamageCalculation(PlayerData mySelf, PlayerData target){

        double damage = (mySelf.getBattleStr() * STR_CONS * (1 + (mySelf.getBattleDex() + mySelf.getBattleAgi()) / 1000)
                        - (target.getBattleDef() * DEF_CONS * (1 + (target.getBattleLuck() / 1000))));
        int lowestDamage = mySelf.getBattleStr() / 10;

        //ダメージ最低保証(攻撃力の10％分は必ず与える)
        if(damage <= lowestDamage){
            damage = lowestDamage;
        }

        //回避処理
        damage = Avoidance(damage,mySelf,target);

        //攻撃が回避されていない(ダメージが0で無い)場合はクリティカル判定
        if(damage != 0){
            damage = Critical(damage,mySelf,target);
        }

        return (int)damage;
    }

    //クリティカル判定
    protected static double Critical(double damage, PlayerData mySelf, PlayerData target){
        double criticalDamage = damage;
        int CriticalTarget = (int)(Math.random()*101) + ((int)(Math.random()*11)+1); //0～100+1～10の範囲で目標値であるCriticalTargetを算出
        int CriticalHit = ((mySelf.getBattleDex() + mySelf.getBattleLuck()) - target.getBattleLuck()) / 10; //自DEXと自LUCKから相手のLUCK値を引いた数の10％を判定値とする
        //CriticalHit値の方がCriticalTargetより上回った場合はダメージ1.5倍
        if(CriticalHit > CriticalTarget ){
            criticalDamage = damage * CRITICAL_CONS;
            mySelf.setActLog("クリティカル！\n");
        }
        return criticalDamage;
    }

    //回避判定
    protected static double Avoidance(double damage,PlayerData mySelf,PlayerData target){
        double finalHit = (mySelf.getHitRate() - target.getEvasionRate()); //最終命中率

        //丸め込み処理
        if(finalHit <= 0){
            finalHit = 25;
        } else if(finalHit > 99){
            finalHit = 99;
        } else {
            finalHit = finalHit + 25;
        }

        //状態異常：暗闇の場合最終命中率半減
        if(mySelf.isStateBlind()){
            finalHit = finalHit / 2;
        }

        int avoidanceTarget = (int)(Math.random()*100); //0～99の目標値を決定

        //最終命中率が目標値未満の場合、ダメージ0(敵が回避)
        if(finalHit < avoidanceTarget){
            damage = 0;
            mySelf.setActLog("しかし攻撃は外れてしまった！\n");
        }
        return damage;
    }

}
