package com.example.mynamebattler.UIPackage.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.PlayerPackage.PlayerData;
import com.example.mynamebattler.R;
/*
　EnemyPartyFragmentを管理するクラス
 */
public class EnemyPartyFragment extends Fragment implements Constant {

    private Bundle bundle;
    private TextView member1NameText,member2NameText,member3NameText;
    private TextView member1StateText, member2StateText, member3StateText;
    private TextView member1JobPAText, member2JobPAText, member3JobPAText;
    private TextView member1HPText,member2HPText,member3HPText;
    private CardView member1CardView,member2CardView,member3CardView;
    private PlayerData member1,member2,member3;

    //Viewの生成(Fragment更新時に必ず更新される様に既存のViewをremove後、設定する)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        super.onCreateView(inflater,container,savedInstanceState);
        container.removeAllViews();
        View rootView = inflater.inflate(R.layout.party_fragment,container,false);
        return rootView;

    }

    //View生成後の処理
    @Override
    public void onViewCreated(View view,Bundle savedInstanceState){
        super.onViewCreated(view,savedInstanceState);

        //Bundleから値を取得
        bundle = getArguments();
        member1 = (PlayerData) bundle.getSerializable(ENEMYPARTY_PLAYER1_KEY);
        member2 = (PlayerData) bundle.getSerializable(ENEMYPARTY_PLAYER2_KEY);
        member3 = (PlayerData) bundle.getSerializable(ENEMYPARTY_PLAYER3_KEY);

        InitBackColor(view);
        InitTextUI(view);

    }

    //背景色の設定
    private void InitBackColor(View view){
        member1CardView = view.findViewById(R.id.member1CardView);
        member2CardView = view.findViewById(R.id.member2CardView);
        member3CardView = view.findViewById(R.id.member3CardView);

        member1CardView.setCardBackgroundColor(Color.parseColor(ENEMY_PARTY_BACK_COLOR));
        member2CardView.setCardBackgroundColor(Color.parseColor(ENEMY_PARTY_BACK_COLOR));
        member3CardView.setCardBackgroundColor(Color.parseColor(ENEMY_PARTY_BACK_COLOR));
    }

    //Bundleから取得した情報を基にテキストを設定
    private void InitTextUI(View view){
            InitMember1Text(view);
            InitMember2Text(view);
            InitMember3Text(view);
    }

    //メンバー1のテキストをセット
    private void InitMember1Text(View view){

        LinearLayout linearLayout = view.findViewById(R.id.member1LinearLayout);

        member1NameText = view.findViewById(R.id.member1NameText);
        member1HPText = view.findViewById(R.id.member1HPText);
        member1StateText = view.findViewById(R.id.member1StateText);
        member1JobPAText = view.findViewById(R.id.member1JobPANameText);

        linearLayout.removeView(member1JobPAText); //敵側のジョブ・PA情報は非表示にする

        member1NameText.setText(member1.getPlayerName());
        member1HPText.setText("〈HP〉\n" + member1.getBattleHp() + "/" + member1.getDefaultHp());
        setStateText(member1,member1StateText);
    }

    //メンバー2のテキストをセット
    private void InitMember2Text(View view){

        LinearLayout linearLayout = view.findViewById(R.id.member2LinearLayout);

        member2NameText = view.findViewById(R.id.member2NameText);
        member2HPText = view.findViewById(R.id.member2HPText);
        member2StateText = view.findViewById(R.id.member2StateText);
        member2JobPAText = view.findViewById(R.id.member2JobPANameText);

        linearLayout.removeView(member2JobPAText); //敵側のジョブ・PA情報は非表示にする

        member2NameText.setText(member2.getPlayerName());
        member2HPText.setText("〈HP〉\n" + member2.getBattleHp() + "/" + member2.getDefaultHp());
        setStateText(member2,member2StateText);
    }

    //メンバー2のテキストをセット
    private void InitMember3Text(View view){

        LinearLayout linearLayout = view.findViewById(R.id.member3LinearLayout);

        member3NameText = view.findViewById(R.id.member3NameText);
        member3HPText = view.findViewById(R.id.member3HPText);
        member3StateText = view.findViewById(R.id.member3StateText);
        member3JobPAText = view.findViewById(R.id.member3JobPANameText);

        linearLayout.removeView(member3JobPAText); //敵側のジョブ・PA情報は非表示にする

        member3NameText.setText(member3.getPlayerName());
        member3HPText.setText("〈HP〉\n" + member3.getBattleHp() + "/" + member3.getDefaultHp());
        setStateText(member3,member3StateText);

    }

    //ステータス情報テキストをセット
    private void setStateText(PlayerData member,TextView stateText){
        if(member.isBadState()){
            stateText.setText("〈状態〉\n");
            if(member.isDead()){
                stateText.setText("戦闘不能 ");
            }
            if(member.isStateSealed()){
                stateText.setText("封印 ");
            }
            if(member.isStateBlind()){
                stateText.setText("暗闇 ");
            }
            if(member.isStatePoison()){
                stateText.setText("毒 ");
            }
            if(member.isStateStun()){
                stateText.setText("気絶 ");
            }
        } else {
            stateText.setText("〈状態〉\n正常");
        }
    }
}
