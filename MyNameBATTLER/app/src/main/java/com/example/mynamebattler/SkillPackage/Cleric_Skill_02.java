package com.example.mynamebattler.SkillPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

/*
　クレリック - スキル2
　オールキュア
 */
public class Cleric_Skill_02 extends Skill implements Serializable {

    @Override
    public void Invoke(PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){
        actionMember.setActLog(actionMember.getPlayerName() + "は" + skillName + "を使用した！\n");
        if(myParty.getSp() < skillCost){
            //SPが足りない場合
            actionMember.setActLog("しかしSPが足らなかった…\n\n");
        } else {
            //スキルを使用した場合
            myParty.SPCountUpdate(-skillCost);

            //自パーティー全員の状態異常を回復する
            for (PlayerData playerData : myParty.getParty()) {
                //回復
                actionMember.setActLog(playerData.getPlayerName() + "の状態異常を解除した！\n\n");
                playerData.CureState();
            }
        }
    }
}
