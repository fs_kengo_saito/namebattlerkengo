package com.example.mynamebattler.PAPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;
import java.util.ArrayList;

/*
　PA：206 プレッシャー
　【発動条件】自パーティーのSP：80以上
　相手全体のSTR＆DEF＆AGL＆DEXを15％ダウンする
 */
public class PA206_PRESSURE implements BasePA,Serializable {

    @Override
    public void Invoke(PlayerData actionMember, Party myParty, Party enemyParty) {
        //発動条件判定
        if(myParty.getSp() >= 80) {
            //PA未発動の場合
            if(!actionMember.isPaCheck()){
                actionMember.setActLog(actionMember.getPlayerName() + "のPA発動！\n" + actionMember.getPlayerName() + "の気迫に相手はたじろいだ！\n\n");
                //各キャラのステータスを減算値を計算し、減算
                for(int i = 0;i < enemyParty.getParty().size();i++){
                    PlayerData playerData = enemyParty.getParty().get(i);
                    playerData.setBattleStr((int)(playerData.getBattleStr() - playerData.getDefaultStr() * 0.15));
                    playerData.setBattleDef((int)(playerData.getBattleDef() - playerData.getDefaultDef() * 0.15));
                    playerData.setBattleAgi((int)(playerData.getBattleAgi() - playerData.getDefaultAgi() * 0.15));
                    playerData.setBattleDex((int)(playerData.getBattleDex() - playerData.getDefaultDex() * 0.15));
                }
                actionMember.setPaCheck(true);
            }
        } else {
            //SP50未満の場合
            //PA発動中の場合
            //ステータスの加算値を取る
            if(actionMember.isPaCheck()){
                for(int i = 0;i < enemyParty.getParty().size();i++){
                    PlayerData playerData = enemyParty.getParty().get(i);
                    playerData.setBattleStr((int)(playerData.getBattleStr() + playerData.getDefaultStr() * 0.15));
                    playerData.setBattleDef((int)(playerData.getBattleDef() + playerData.getDefaultDef() * 0.15));
                    playerData.setBattleAgi((int)(playerData.getBattleAgi() + playerData.getDefaultAgi() * 0.15));
                    playerData.setBattleDex((int)(playerData.getBattleDex() + playerData.getDefaultDex() * 0.15));
                }
                actionMember.setPaCheck(false);
            }
        }
    }
}
