package com.example.mynamebattler.SkillPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　ファイター - スキル1
　渾身の一撃
 */
public class Fighter_Skill_01 extends Skill implements Serializable {

    @Override
    public void Invoke(PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){
        actionMember.setActLog(actionMember.getPlayerName() + "は" + skillName + "を使用した！\n");
        if(myParty.getSp() < skillCost){
            //SPが足りない場合
            actionMember.setActLog("しかしSPが足らなかった…\n\n");
        } else {
            //スキルを使用した場合
            myParty.SPCountUpdate(-skillCost);
            actionMember.setActLog(target.getPlayerName() + "へ強力な一撃！\n");

            //STRを1.25倍にして攻撃
            double str = actionMember.getBattleStr() * 1.25;
            actionMember.setBattleStr((int) str);
            actionMember.Attack(actionMember,target);

            //STRを元に戻す
            actionMember.setBattleStr(actionMember.getDefaultStr());
        }
    }
}
