package com.example.mynamebattler.UIPackage.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.mynamebattler.DBPackage.DBManager;
import com.example.mynamebattler.Constant;
import com.example.mynamebattler.ManagerPackage.CharacterDataManager;
import com.example.mynamebattler.ManagerPackage.ImageManager;
import com.example.mynamebattler.ManagerPackage.PartyManager;
import com.example.mynamebattler.ManagerPackage.SoundManager;
import com.example.mynamebattler.PartyPackage.MyParty;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.R;
import com.example.mynamebattler.UIPackage.Fragment.HeaderFragment;
import com.example.mynamebattler.UIPackage.Fragment.PartyCharacterListFragment;

import java.util.ArrayList;
/*
　パーティー編成画面の表示及びUI処理を行うクラス
 */
public class PartyFormationActivity extends AppCompatActivity implements Constant{

    private ArrayList<Integer> allCharaId;
    private ArrayList<String> allCharaName,allCharaJob,allCharaPA;
    private Party myParty;

    private CharacterDataManager characterDataManager;
    private PartyManager partyManager;
    private SoundManager soundManager;
    private ImageManager imageManager;
    private Bundle bundle;
    private PartyCharacterListFragment partyCharacterListFragment;
    private HeaderFragment headerFragment;

    //端末の戻るを禁止する
    @Override
    public void onBackPressed(){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.party_formation_activity);

        //Bundleの初期化
        bundle = new Bundle();

        //各種インスタンスを取得
        characterDataManager = CharacterDataManager.getInstance();
        partyManager = PartyManager.getInstance();
        soundManager = SoundManager.getInstance();
        imageManager = ImageManager.getInstance();

        //パーティーを初期化
        partyManager.ResetParty();
        myParty = new MyParty(partyManager.getMyParty());

        //各種UIを設定
        InitCharacterListUI();
        InitHeaderFragment();
        InitButtonUI();
    }

    //Activityが隠れた時に呼び出し
    @Override
    protected void onPause(){
        super.onPause();
        soundManager.BGMPause();
    }

    //Activityが前面に表示された時に呼び出し
    protected void onResume(){
        super.onResume();
        //BGMの再開
        soundManager.BGMReStart();
    }

    //Activityが破棄されるタイミングでサービスを終了させる
    @Override
    protected void onDestroy(){
        super.onDestroy();
        soundManager.BGMStop();
    }


    //キャラクターリストの表示＆処理
    private void InitCharacterListUI(){
        //DBから各種データを取得
        allCharaId = characterDataManager.GetAllCharacterID();
        allCharaName = characterDataManager.GetAllCharacterName();
        allCharaJob = characterDataManager.GetAllCharacterJob();
        allCharaPA = characterDataManager.GetAllCharacterPA();

        //バンドルにデータを格納
        bundle.putIntegerArrayList(CHARACTER_ID_LIST_KEY,allCharaId);
        bundle.putStringArrayList(CHARACTER_NAME_LIST_KEY,allCharaName);
        bundle.putStringArrayList(CHARACTER_JOB_LIST_KEY,allCharaJob);
        bundle.putStringArrayList(CHARACTER_PA_LIST_KEY,allCharaPA);

        //characterListFragmentを作成して表示
        partyCharacterListFragment = new PartyCharacterListFragment();
        partyCharacterListFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.partyCharacterListFlame,partyCharacterListFragment);
        transaction.commit();
    }

    //HeaderFragmentの表示
    private void InitHeaderFragment(){

        bundle.putString(BACK_ACT_CHECK_KEY,BACK_TITLE_ACT_KEY);
        headerFragment = new HeaderFragment();
        headerFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.headerFragmentFrame,headerFragment);
        transaction.commit();
    }

    //ボタンの設定＆処理
    private void InitButtonUI(){

        //バトル開始ボタン押下時の動作
        ImageButton readyBtn = findViewById(R.id.SendBattleReadyActButton);
        Bitmap btnImage2 = imageManager.GetImageBitmap(this,READY_BUTTON_IMAGE);
        readyBtn.setImageBitmap(btnImage2);
        readyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myParty == null || myParty.getParty().size() != PARTY_INIT_LIMIT){
                    //編成人数が足りない場合はエラー表示
                    new AlertDialog.Builder(PartyFormationActivity.this)
                            .setTitle("パーティー人数エラー")
                            .setMessage("パーティーの編成人数が足りません。")
                            .setNegativeButton("OK",null)
                            .show();
                } else {
                    //バトル準備画面へ遷移
                    Intent intent = new Intent(getApplication(), BattleReadyActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

}
