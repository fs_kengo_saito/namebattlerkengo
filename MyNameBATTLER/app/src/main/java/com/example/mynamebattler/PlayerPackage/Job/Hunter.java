package com.example.mynamebattler.PlayerPackage.Job;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
職業：ハンター(Playerクラスを継承)
 */
public class Hunter extends PlayerData implements Constant, Serializable {

    public Hunter(String name){
        super(name);
    }

    //名前から取得したステータスでCharacter作成
    @Override
    protected void CharaCreate(){
        int randomIndex = (int)(Math.random()*10);
        defaultHp = (GetNumber(randomIndex,STATUS_RANGE_BASE) + STATUS_RANGE_D) * STATUS_HP_FACTOR;
        defaultStr = GetNumber(randomIndex+1,STATUS_RANGE_BASE) + STATUS_RANGE_C;
        defaultDef = GetNumber(randomIndex+2,STATUS_RANGE_BASE) + STATUS_RANGE_E;
        defaultAgi = GetNumber(randomIndex+3,STATUS_RANGE_BASE) + STATUS_RANGE_A;
        defaultDex = GetNumber(randomIndex+4,STATUS_RANGE_BASE) + STATUS_RANGE_A;
        defaultLuck = GetNumber(randomIndex+5,STATUS_RANGE_BASE) + STATUS_RANGE_C;
    }

    //JA用メソッド
    @Override
    public void JA(PlayerData actionMember, Party myParty, Party enemyParty){

        //自パーティーのSP70以上で発動
        if(myParty.getSp() >= 70){
            //JA未発動ならステータス更新
            if(!JACheck){
                double dex = defaultDex * 1.5;
                setBattleDex((int)dex);
                actLog.append(getPlayerName() + "のJA発動！\n驚異の集中力で自身のDEXが50％アップ！\n\n");
                setJACheck(true);
            }
        } else {
            //自パーティーのSP70未満且つJA発動中の場合、ステータス更新
            if(JACheck){
                setBattleDex(defaultDex);
                setJACheck(false);
            }
        }
    }

}
